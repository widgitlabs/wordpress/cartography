<?php
/**
 * Plugin Name:     Cartography
 * Plugin URI:      https://widgitlabs.com
 * Description:     A better map builder.
 * Author:          Widgit Team
 * Author URI:      https://widgitlabs.com
 * Version:         1.0.0
 * Text Domain:     cartography
 * Domain Path:     languages
 *
 * @package         Cartography
 * @author          Dan Griffiths <dgriffiths@widgitlabs.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Cartography' ) ) {

	/**
	 * Main Cartography class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Cartography {

		/**
		 * The one true Cartography
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Cartography $instance The one true Cartography.
		 */
		private static $instance;


		/**
		 * The roles object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         Cartography_Roles $roles The roles object.
		 */
		public $roles;


		/**
		 * The HTML helper object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         Cartography_HTML_Elements $html The HTML elements helper.
		 */
		public $html;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object.
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Cartography
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Cartography ) ) {
				self::$instance = new Cartography();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'cartography' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'cartography' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'CARTOGRAPHY_VER' ) ) {
				define( 'CARTOGRAPHY_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'CARTOGRAPHY_DIR' ) ) {
				define( 'CARTOGRAPHY_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'CARTOGRAPHY_URL' ) ) {
				define( 'CARTOGRAPHY_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'CARTOGRAPHY_FILE' ) ) {
				define( 'CARTOGRAPHY_FILE', __FILE__ );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $cartography_options;

			// Conditionally load the settings library.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				if ( file_exists( CARTOGRAPHY_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php' ) ) {
					require_once CARTOGRAPHY_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php';
				}
			}

			require_once CARTOGRAPHY_DIR . 'includes/admin/settings/register.php';

			self::$instance->settings = new Simple_Settings( 'cartography', 'providers', array( 'sysinfo' ) );
			$cartography_options      = self::$instance->settings->get_settings();

			require_once CARTOGRAPHY_DIR . 'includes/ajax-functions.php';
			require_once CARTOGRAPHY_DIR . 'includes/country-functions.php';
			require_once CARTOGRAPHY_DIR . 'includes/misc-functions.php';
			require_once CARTOGRAPHY_DIR . 'includes/plugin-compatibility.php';
			require_once CARTOGRAPHY_DIR . 'includes/post-types.php';
			require_once CARTOGRAPHY_DIR . 'includes/scripts.php';
			require_once CARTOGRAPHY_DIR . 'includes/shortcodes.php';

			if ( is_admin() ) {
				require_once CARTOGRAPHY_DIR . 'includes/admin/location/meta-box.php';
				require_once CARTOGRAPHY_DIR . 'includes/admin/map/meta-box.php';
			}
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'cartography_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'cartography' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'cartography', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/cartography/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/cartography/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/cartography folder.
				load_textdomain( 'cartography', $mofile_global );
				return;
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/cartography/languages/ folder.
				load_textdomain( 'cartography', $mofile_local );
				return;
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/cartography/ folder.
				load_textdomain( 'cartography', $mofile_core );
				return;
			}

			// Load the default language files.
			load_plugin_textdomain( 'cartography', false, $lang_dir );
		}
	}
}


/**
 * The main function responsible for returning the one true Cartography
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $cartography = cartography(); ?>
 *
 * @since       1.0.0
 * @return      Cartography The one true Cartography
 */
function cartography() {
	return Cartography::instance();
}

// Get things started.
cartography();
