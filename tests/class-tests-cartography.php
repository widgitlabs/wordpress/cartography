<?php
/**
 * Cartography unit tests
 *
 * @package     Cartography\Tests
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Cartography unit tests
 *
 * @since       1.0.0
 *
 * @uses ::cartography
 */
class Tests_Cartography extends WP_UnitTestCase {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = cartography();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}


	/**
	 * Test cartography
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 * @covers      ::cartography
	 * @covers      Cartography::instance
	 */
	public function test_cartography() {
		$this->assertEquals( '1.0.0', CARTOGRAPHY_VER );
	}
}
