<?php
/**
 * Plugin compatibility
 *
 * @package     Cartography\Compat
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Checks if a caching plugin is active
 *
 * @since       1.0.0
 * @return      bool $caching True if caching plugin is enabled, false otherwise.
 */
function cartography_is_caching_plugin_active() {
	$caching = ( function_exists( 'wpsupercache_site_admin' ) || defined( 'W3TC' ) || function_exists( 'rocket_init' ) ) || defined( 'WPHB_VERSION' );

	return apply_filters( 'cartography_is_caching_plugin_active', $caching );
}
