<?php
/**
 * Scripts
 *
 * @package     Cartography\Scripts
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Load scripts
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_enqueue_scripts() {
	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	// Setup versioning for internal assets.
	$css_ver = gmdate( 'ymd-Gis', filemtime( CARTOGRAPHY_DIR . 'assets/css/cartography' . $suffix . '.css' ) );

	wp_register_style( 'cartography', CARTOGRAPHY_URL . 'assets/css/cartography' . $suffix . '.css', array(), CARTOGRAPHY_VER . '-' . $css_ver );
	wp_enqueue_style( 'cartography' );
}
add_action( 'wp_enqueue_scripts', 'cartography_enqueue_scripts' );


/**
 * Load admin scripts
 *
 * @since       1.0.0
 * @return      void
 * @todo        Pick a maps API version and enforce it.
 */
function cartography_admin_enqueue_scripts() {
	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	// Setup versioning for internal assets.
	$css_ver = gmdate( 'ymd-Gis', filemtime( CARTOGRAPHY_DIR . 'assets/css/admin' . $suffix . '.css' ) );
	$js_ver  = gmdate( 'ymd-Gis', filemtime( CARTOGRAPHY_DIR . 'assets/js/admin' . $suffix . '.js' ) );

	wp_register_style( 'cartography', CARTOGRAPHY_URL . 'assets/css/admin' . $suffix . '.css', array(), CARTOGRAPHY_VER . '-' . $css_ver );
	wp_enqueue_style( 'cartography' );

	wp_register_style( 'cartography-select2', CARTOGRAPHY_URL . 'assets/js/select2/css/select2.min.css', array(), '4.1.0' );
	wp_enqueue_style( 'cartography-select2' );
	wp_enqueue_script( 'cartography-select2', CARTOGRAPHY_URL . 'assets/js/select2/js/select2.min.js', array(), '4.1.0', false );

	$api_key = cartography()->settings->get_option( 'google_maps_api_key', false );
	$api_key = $api_key ? '?key=' . $api_key : '';

	wp_enqueue_script( 'cartography-gmaps', esc_url( 'https://maps.googleapis.com/maps/api/js' . $api_key ), array(), '', true ); // phpcs:ignore

	wp_enqueue_script( 'cartography', CARTOGRAPHY_URL . 'assets/js/admin' . $suffix . '.js', array( 'cartography-gmaps', 'cartography-select2' ), CARTOGRAPHY_VER . '-' . $js_ver, true );
}
add_action( 'admin_enqueue_scripts', 'cartography_admin_enqueue_scripts' );
