<?php
/**
 * AJAX Functions
 *
 * @package     Cartography\Functions\AJAX
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Get AJAX URL
 *
 * @since       1.0.0
 * @return      string URL to the AJAX file to call during AJAX requests.
 */
function cartography_get_ajax_url() {
	$scheme = defined( 'FORCE_SSL_ADMIN' ) && FORCE_SSL_ADMIN ? 'https' : 'admin';

	$current_url = cartography_get_current_page_url();
	$ajax_url    = admin_url( 'admin-ajax.php', $scheme );

	if ( preg_match( '/^https/', $current_url ) && ! preg_match( '/^https/', $ajax_url ) ) {
		$ajax_url = preg_replace( '/^http/', 'https', $ajax_url );
	}

	return apply_filters( 'cartography_ajax_url', $ajax_url );
}


/**
 * Retrieve a states drop down
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_ajax_get_states_field() {
	$nonce          = isset( $_POST['nonce'] ) ? sanitize_text_field( $_POST['nonce'] ) : '';
	$nonce_verified = wp_verify_nonce( $nonce, 'cartography-location-country-nonce' );

	if ( false !== $nonce_verified ) {
		$post = wp_unslash( $_POST );

		if ( empty( $post['country'] ) ) {
			$post['country'] = 'US';
		}

		$states = cartography_get_states( $post['country'] );

		if ( ! empty( $states ) ) {
			$response = '<select name="location_address[state]" id="cartography-location-state">';

			foreach ( $states as $state_code => $state_name ) {
				$response .= '<option value="' . esc_attr( $state_code ) . '">' . esc_html( $state_name ) . '</option>';
			}

			$response .= '</select>';
		} else {
			$response = 'no_states';
		}

		// TODO: We need a custom kses handler for this.
		echo $response; // phpcs:ignore
	}

	die();
}
add_action( 'wp_ajax_cartography_get_states', 'cartography_ajax_get_states_field' );
add_action( 'wp_ajax_nopriv_cartography_get_states', 'cartography_ajax_get_states_field' );
