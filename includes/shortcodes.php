<?php
/**
 * Shortcodes
 *
 * @package     Cartography\Shortcodes
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Display a map
 *
 * @since       1.0.0
 * @param       array       $atts Attributes passed to the shortcode.
 * @param       string|null $content The shortcode content.
 * @return      string      $the_shortcode The generated shortcode HTML.
 * @todo        Pick a maps API version and enforce it.
 */
function cartography_map_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(),
		$atts,
		'cartography_map'
	);

	$locations = new WP_Query(
		array(
			'post_type'      => 'location',
			'posts_per_page' => -1,
		)
	);

	// Bail if there are no locations.
	if ( 1 > count( $locations ) ) {
		return;
	}

	$type  = cartography()->settings->get_option( 'google_maps_view', 'default' );
	$zoom  = cartography()->settings->get_option( 'google_maps_zoom', 8 );
	$pin   = cartography()->settings->get_option( 'google_maps_pin', '' );
	$style = cartography()->settings->get_option( 'snazzy_maps_style', '[]' );

	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	// Setup versioning for internal assets.
	$js_ver = gmdate( 'ymd-Gis', filemtime( CARTOGRAPHY_DIR . 'assets/js/cartography' . $suffix . '.js' ) );

	$api_key = cartography()->settings->get_option( 'google_maps_api_key', false );
	$api_key = $api_key ? '?key=' . $api_key : '';

	ob_start();

	wp_enqueue_script( 'cartography-google-maps', esc_url( 'https://maps.googleapis.com/maps/api/js' . $api_key ), array(), '', true ); // phpcs:ignore

	wp_enqueue_script( 'cartography', CARTOGRAPHY_URL . 'assets/js/cartography' . $suffix . '.js', array( 'cartography-google-maps' ), CARTOGRAPHY_VER . '-' . $js_ver, true );
	wp_localize_script(
		'cartography',
		'cartography_vars',
		array(
			'geocode_api'  => esc_url( 'http://maps.googleapis.com/maps/api/geocode/json' . $api_key ),
			'distance_api' => esc_url( 'https://maps.googleapis.com/maps/api/distancematrix/json' . $api_key ),
		)
	);
	?>
	<div class="cartography-filters">
		<div class="cartography-filter-half">
			<input type="text" id="cartography-filter-location" placeholder="<?php esc_attr_e( 'Your Location (Zip code or City & State)', 'cartography' ); ?>" />
			<span id="cartography-get-location"><i class="fas fa-crosshairs"></i></span>
		</div>
		<div class="cartography-filter-half">
			<div class="cartography-filter-half cartography-filter-radius-wrap">
				<select id="cartography-filter-radius">
					<option value="0"><?php esc_html_e( 'No Limit', 'cartography' ); ?></option>
					<option value="50"><?php esc_html_e( '50 Miles', 'cartography' ); ?></option>
					<option value="100"><?php esc_html_e( '100 Miles', 'cartography' ); ?></option>
					<option value="200"><?php esc_html_e( '200 Miles', 'cartography' ); ?></option>
				</select>
			</div>
			<div class="cartography-filter-half">
				<input type="button" id="cartography-filter-submit" value="<?php esc_attr_e( 'Search', 'cartography' ); ?>" />
			</div>
		</div>
	</div>
	<div id="cartography-wrap">
		<div class="cartography-location-list">
			<?php foreach ( $locations->posts as $location ) : ?>
				<?php
				$location_address = get_post_meta( $location->ID, 'location_address', true );
				$location_coords  = get_post_meta( $location->ID, 'location_coords', true );
				$location_contact = get_post_meta( $location->ID, 'location_contact', true );
				$title            = get_the_title( $location->ID );
				$thumbnail        = get_the_post_thumbnail_url( $location->ID, 'medium' );
				?>
				<div id="cartography-location-<?php echo esc_attr( $location->ID ); ?>"
					class="cartography-location-wrap"
					data-id="<?php echo esc_attr( $location->ID ); ?>"
					data-latitude="<?php echo esc_attr( $location_coords['latitude'] ); ?>"
					data-longitude="<?php echo esc_attr( $location_coords['longitude'] ); ?>">
					<div class="cartography-location-title">
						<a class="cartography-location-title-link" title="<?php echo esc_attr( get_the_title( $location->ID ) ); ?>" data-id="<?php echo esc_attr( $location->ID ); ?>">
							<?php echo esc_html( $title ); ?>
						</a>
					</div>
					<div class="cartography-location-address">
						<div>
							<span><?php echo esc_html( $location_address['address1'] ); ?></span>
						</div>
						<?php if ( ! empty( $location_address['address2'] ) ) : ?>
							<div>
								<span><?php echo esc_html( $location_address['address2'] ); ?></span>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $location_address['city'] ) || ! empty( $location_address['state'] ) || ! empty( $location_address['zip'] ) ) : ?>
							<div>
								<?php if ( ! empty( $location_address['city'] ) ) : ?>
									<span>
										<?php
										echo esc_html( $location_address['city'] );
										echo esc_html( apply_filters( 'cartography_location_details_city_sep', ',' ) );
										?>
									</span>
								<?php endif; ?>

								<?php if ( ! empty( $location_address['state'] ) ) : ?>
									<span>
										<?php
										echo esc_html( $location_address['state'] );
										echo esc_html( apply_filters( 'cartography_location_details_state_sep', '' ) );
										?>
									</span>
								<?php endif; ?>

								<?php if ( ! empty( $location_address['zip'] ) ) : ?>
									<span>
										<?php echo esc_html( $location_address['zip'] ); ?>
									</span>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
					<?php if ( ! empty( $location_contact['name'] ) || ! empty( $location_contact['email'] ) || ! empty( $location_contact['website'] ) || ! empty( $location_contact['phone'] ) ) : ?>
						<div class="cartography-location-contact">
							<?php if ( ! empty( $location_contact['name'] ) ) : ?>
								<span class="cartography-location-contact-name">
									<?php echo esc_html( $location_contact['name'] ); ?>
								</span>
							<?php endif; ?>
							<?php if ( ! empty( $location_contact['email'] ) ) : ?>
								<span class="cartography-location-contact-email">
									<span class="dashicons dashicons-email-alt"></span>
									<a href="mailto:<?php echo esc_url( $location_contact['email'] ); ?>"><?php echo esc_html( $location_contact['email'] ); ?></a>
								</span>
							<?php endif; ?>
							<?php if ( ! empty( $location_contact['phone'] ) ) : ?>
								<span class="cartography-location-contact-phone">
									<span class="dashicons dashicons-smartphone"></span>
									<a href="tel:<?php echo esc_html( $location_contact['phone'] ); ?>"><?php echo esc_html( $location_contact['phone'] ); ?></a>
								</span>
							<?php endif; ?>
							<?php if ( ! empty( $location_contact['website'] ) ) : ?>
								<span class="cartography-location-contact-website">
									<span class="dashicons dashicons-admin-site"></span>
									<a href="tel:<?php echo esc_html( $location_contact['website'] ); ?>"><?php echo esc_html( $location_contact['website'] ); ?></a>
								</span>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<div class="cartography-location-details-wrap">
						<div class="cartography-location-details">
							<?php if ( $thumbnail ) : ?>
								<div class="cartography-location-details-thumbnail">
									<img src="<?php echo esc_url( $thumbnail ); ?>" alt="<?php echo esc_attr( $title ); ?>" />
								</div>
							<?php endif; ?>
							<div class="cartography-location-details-title">
								<?php echo esc_html( $title ); ?>
							</div>
							<div class="cartography-location-details-address">
								<div>
									<span><?php echo esc_html( $location_address['address1'] ); ?></span>
								</div>
								<?php if ( ! empty( $location_address['address2'] ) ) : ?>
									<div>
										<span><?php echo esc_html( $location_address['address2'] ); ?></span>
									</div>
								<?php endif; ?>
								<?php if ( ! empty( $location_address['city'] ) || ! empty( $location_address['state'] ) || ! empty( $location_address['zip'] ) ) : ?>
									<div>
										<?php if ( ! empty( $location_address['city'] ) ) : ?>
											<span>
												<?php
												echo esc_html( $location_address['city'] );
												echo esc_html( apply_filters( 'cartography_location_details_city_sep', ',' ) );
												?>
											</span>
										<?php endif; ?>

										<?php if ( ! empty( $location_address['state'] ) ) : ?>
											<span>
												<?php
												echo esc_html( $location_address['state'] );
												echo esc_html( apply_filters( 'cartography_location_details_state_sep', '' ) );
												?>
											</span>
										<?php endif; ?>

										<?php if ( ! empty( $location_address['zip'] ) ) : ?>
											<span>
												<?php echo esc_html( $location_address['zip'] ); ?>
											</span>
										<?php endif; ?>
									</div>
								<?php endif; ?>
							</div>
							<?php if ( ! empty( $location->post_content ) ) : ?>
								<div class="cartography-location-details-content"><?php echo wp_kses_post( $location->post_content ); ?></div>
							<?php endif; ?>
							<?php if ( ! empty( $location_contact['name'] ) || ! empty( $location_contact['email'] ) || ! empty( $location_contact['website'] ) || ! empty( $location_contact['phone'] ) ) : ?>
								<div class="cartography-location-details-contact">
									<?php if ( ! empty( $location_contact['name'] ) ) : ?>
										<span class="cartography-location-details-contact-name">
											<?php echo esc_html( $location_contact['name'] ); ?>
										</span>
									<?php endif; ?>
									<?php if ( ! empty( $location_contact['email'] ) ) : ?>
										<span class="cartography-location-details-contact-email">
											<span class="dashicons dashicons-email-alt"></span>
											<a href="mailto:<?php echo esc_url( $location_contact['email'] ); ?>"><?php echo esc_html( $location_contact['email'] ); ?></a>
										</span>
									<?php endif; ?>
									<?php if ( ! empty( $location_contact['phone'] ) ) : ?>
										<span class="cartography-location-details-contact-phone">
											<span class="dashicons dashicons-smartphone"></span>
											<a href="tel:<?php echo esc_html( $location_contact['phone'] ); ?>"><?php echo esc_html( $location_contact['phone'] ); ?></a>
										</span>
									<?php endif; ?>
									<?php if ( ! empty( $location_contact['website'] ) ) : ?>
										<span class="cartography-location-details-contact-website">
											<span class="dashicons dashicons-admin-site"></span>
											<a href="tel:<?php echo esc_html( $location_contact['website'] ); ?>"><?php echo esc_html( $location_contact['website'] ); ?></a>
										</span>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<div class="cartography-no-locations" style="display: none;"><?php esc_html_e( 'No studies match the specified search criteria.', 'cartography' ); ?></div>
		</div>
		<div id="cartography-location-map"
			data-maptype="<?php echo esc_attr( $type ); ?>"
			data-zoom="<?php echo esc_attr( absint( $zoom ) ); ?>"
			data-pin="<?php echo esc_attr( esc_url( $pin ) ); ?>"
			data-style="<?php echo esc_attr( $style ); ?>"></div>
	</div>
	<?php
	$the_shortcode = ob_get_clean();

	return $the_shortcode;
}
add_shortcode( 'cartography_map', 'cartography_map_shortcode' );
