<?php
/**
 * Misc helper functions
 *
 * @package     Cartography\Functions\Misc
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Get the current page URL
 *
 * @since       1.0.0
 * @param       bool $nocache  If we should bust cache on the returned URL.
 * @return      string $page_url Current page URL.
 */
function cartography_get_current_page_url( $nocache = false ) {
	global $wp;

	if ( get_option( 'permalink_structure' ) ) {
		$base = trailingslashit( home_url( $wp->request ) );
	} else {
		$base = add_query_arg( $wp->query_string, '', trailingslashit( home_url( $wp->request ) ) );
		$base = remove_query_arg( array( 'post_type', 'name' ), $base );
	}

	$scheme = is_ssl() ? 'https' : 'http';
	$uri    = set_url_scheme( $base, $scheme );

	if ( is_front_page() ) {
		$uri = home_url( '/' );
	}

	$uri = apply_filters( 'cartography_get_current_page_url', $uri );

	if ( $nocache ) {
		$uri = cartography_add_cache_busting( $uri );
	}

	return $uri;
}


/**
 * Adds the 'nocache' parameter to the provided URL
 *
 * @since       1.0.0
 * @param       string $url The URL being requested.
 * @return      string The URL with cache busting added or not.
 */
function cartography_add_cache_busting( $url = '' ) {
	if ( cartography_is_caching_plugin_active() ) {
		$url = add_query_arg( 'nocache', 'true', $url );
	}

	return $url;
}
