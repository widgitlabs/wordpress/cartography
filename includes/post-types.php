<?php
/**
 * Post Type Functions
 *
 * @package     Cartography\Post_Types
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Registers and sets up the maps custom post type
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_setup_post_types() {
	$archives = defined( 'CARTOGRAPHY_DISABLE_ARCHIVE' ) && CARTOGRAPHY_DISABLE_ARCHIVE ? false : true;

	// Map post type.
	$rewrite = defined( 'CARTOGRAPHY_DISABLE_REWRITE' ) && CARTOGRAPHY_DISABLE_REWRITE ? false : array(
		'slug'       => 'map',
		'with_front' => false,
	);

	$map_labels = apply_filters(
		'cartography_map_labels',
		array(
			/* translators: Post type name. %s represents plural name. */
			'name'                  => _x( '%2$s', 'post type name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			/* translators: Post type name. %s represents singular name. */
			'singular_name'         => _x( '%1$s', 'singular map post type name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			'add_new'               => __( 'Add New', 'cartography' ),
			/* translators: Add New button label. %s represents singular name. */
			'add_new_item'          => __( 'Add New %1$s', 'cartography' ),
			/* translators: Edit button label. %s represents singular name. */
			'edit_item'             => __( 'Edit %1$s', 'cartography' ),
			/* translators: New post label. %s represents singular name. */
			'new_item'              => __( 'New %1$s', 'cartography' ),
			/* translators: All posts label. %s represents plural name. */
			'all_items'             => __( 'All %2$s', 'cartography' ),
			/* translators: View post label. %s represents singular name. */
			'view_item'             => __( 'View %1$s', 'cartography' ),
			/* translators: Search label. %s represents plural name. */
			'search_items'          => __( 'Search %2$s', 'cartography' ),
			/* translators: Not found label. %s represents plural name. */
			'not_found'             => __( 'No %2$s found', 'cartography' ),
			/* translators: Not found in trash label. %s represents plural name. */
			'not_found_in_trash'    => __( 'No %2$s found in Trash', 'cartography' ),
			'parent_item_colon'     => '',
			/* translators: Menu label. %s represents plural name. */
			'menu_name'             => _x( '%2$s', 'map post type menu name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			/* translators: Featured image label. %s represents singular name. */
			'featured_image'        => __( '%1$s Image', 'cartography' ),
			/* translators: Set featured image label. %s represents singular name. */
			'set_featured_image'    => __( 'Set %1$s Image', 'cartography' ),
			/* translators: Remove featured image label. %s represents singular name. */
			'remove_featured_image' => __( 'Remove %1$s Image', 'cartography' ),
			/* translators: Use as featured image label. %s represents singular name. */
			'use_featured_image'    => __( 'Use as %1$s Image', 'cartography' ),
			/* translators: Attributes label. %s represents singular name. */
			'attributes'            => __( '%1$s Attributes', 'cartography' ),
			/* translators: Filter list label. %s represents plural name. */
			'filter_items_list'     => __( 'Filter %2$s list', 'cartography' ),
			/* translators: List navigation label. %s represents plural name. */
			'items_list_navigation' => __( '%2$s list navigation', 'cartography' ),
			/* translators: List label. %s represents plural name. */
			'items_list'            => __( '%2$s list', 'cartography' ),
		)
	);

	$map_singular = cartography_get_label_singular();
	$map_plural   = cartography_get_label_plural();

	foreach ( $map_labels as $key => $value ) {
		$map_labels[ $key ] = sprintf( $value, $map_singular, $map_plural );
	}

	$map_args = array(
		'labels'             => $map_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'menu_icon'          => 'dashicons-location',
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'map',
		'map_meta_cap'       => true,
		'has_archive'        => $archives,
		'hierarchical'       => false,
		'supports'           => apply_filters(
			'cartography_map_supports',
			array(
				'title',
				'revisions',
				'author',
			)
		),
	);
	register_post_type( 'map', apply_filters( 'cartography_map_post_type_args', $map_args ) );

	// Location post type.
	$rewrite = defined( 'CARTOGRAPHY_DISABLE_REWRITE' ) && CARTOGRAPHY_DISABLE_REWRITE ? false : array(
		'slug'       => 'location',
		'with_front' => false,
	);

	$location_labels = apply_filters(
		'cartography_location_labels',
		array(
			/* translators: Post type name. %s represents plural name. */
			'name'                  => _x( '%2$s', 'post type name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			/* translators: Post type name. %s represents singular name. */
			'singular_name'         => _x( '%1$s', 'singular location post type name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			'add_new'               => __( 'Add New', 'cartography' ),
			/* translators: Add New button label. %s represents singular name. */
			'add_new_item'          => __( 'Add New %1$s', 'cartography' ),
			/* translators: Edit button label. %s represents singular name. */
			'edit_item'             => __( 'Edit %1$s', 'cartography' ),
			/* translators: New post label. %s represents singular name. */
			'new_item'              => __( 'New %1$s', 'cartography' ),
			/* translators: All posts label. %s represents plural name. */
			'all_items'             => __( '%2$s', 'cartography' ), // phpcs:ignore
			/* translators: View post label. %s represents singular name. */
			'view_item'             => __( 'View %1$s', 'cartography' ),
			/* translators: Search label. %s represents plural name. */
			'search_items'          => __( 'Search %2$s', 'cartography' ),
			/* translators: Not found label. %s represents plural name. */
			'not_found'             => __( 'No %2$s found', 'cartography' ),
			/* translators: Not found in trash label. %s represents plural name. */
			'not_found_in_trash'    => __( 'No %2$s found in Trash', 'cartography' ),
			'parent_item_colon'     => '',
			/* translators: Menu label. %s represents plural name. */
			'menu_name'             => _x( '%2$s', 'map post type menu name', 'cartography' ), // phpcs:ignore WordPress.WP.I18n.NoEmptyStrings
			/* translators: Featured image label. %s represents singular name. */
			'featured_image'        => __( '%1$s Image', 'cartography' ),
			/* translators: Set featured image label. %s represents singular name. */
			'set_featured_image'    => __( 'Set %1$s Image', 'cartography' ),
			/* translators: Remove featured image label. %s represents singular name. */
			'remove_featured_image' => __( 'Remove %1$s Image', 'cartography' ),
			/* translators: Use as featured image label. %s represents singular name. */
			'use_featured_image'    => __( 'Use as %1$s Image', 'cartography' ),
			/* translators: Attributes label. %s represents singular name. */
			'attributes'            => __( '%1$s Attributes', 'cartography' ),
			/* translators: Filter list label. %s represents plural name. */
			'filter_items_list'     => __( 'Filter %2$s list', 'cartography' ),
			/* translators: List navigation label. %s represents plural name. */
			'items_list_navigation' => __( '%2$s list navigation', 'cartography' ),
			/* translators: List label. %s represents plural name. */
			'items_list'            => __( '%2$s list', 'cartography' ),
		)
	);

	$location_singular = cartography_get_label_singular( false, 'location' );
	$location_plural   = cartography_get_label_plural( false, 'location' );

	foreach ( $location_labels as $key => $value ) {
		$location_labels[ $key ] = sprintf( $value, $location_singular, $location_plural );
	}

	$location_args = array(
		'labels'             => $location_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php?post_type=map',
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'location',
		'map_meta_cap'       => true,
		'has_archive'        => $archives,
		'hierarchical'       => false,
		'supports'           => apply_filters(
			'cartography_location_supports',
			array(
				'title',
				'editor',
				'thumbnail',
				'revisions',
				'author',
			)
		),
	);
	register_post_type( 'location', apply_filters( 'cartography_location_post_type_args', $location_args ) );
}
add_action( 'init', 'cartography_setup_post_types', 1 );


/**
 * Get Default Labels
 *
 * @since       1.0.0
 * @param       string $type The post type.
 * @return      array $defaults Default labels.
 */
function cartography_get_default_labels( $type = '' ) {
	$defaults = apply_filters(
		'cartography_default_labels',
		array(
			'map'      => array(
				'singular' => __( 'Map', 'cartography' ),
				'plural'   => __( 'Maps', 'cartography' ),
			),
			'location' => array(
				'singular' => __( 'Location', 'cartography' ),
				'plural'   => __( 'Locations', 'cartography' ),
			),
		)
	);

	if ( ! empty( $type ) && array_key_exists( $type, $defaults ) ) {
		$defaults = $defaults[ $type ];
	}

	return $defaults;
}


/**
 * Get Singular Label
 *
 * @since       1.0.0
 * @param       bool   $lowercase Whether or not to return lowercase.
 * @param       string $type The post type.
 * @return      string $defaults['singular'] Singular label.
 */
function cartography_get_label_singular( $lowercase = false, $type = 'map' ) {
	$defaults = cartography_get_default_labels( $type );

	return ( $lowercase ) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}


/**
 * Get Plural Label
 *
 * @since       1.0.0
 * @param       bool   $lowercase Whether or not to return the string lower-cased.
 * @param       string $type The post type.
 * @return      string $defaults['plural'] Plural label.
 */
function cartography_get_label_plural( $lowercase = false, $type = 'map' ) {
	$defaults = cartography_get_default_labels( $type );

	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}


/**
 * Change default "Enter title here" input
 *
 * @since       1.0.0
 * @param       string $title Default title placeholder text.
 * @return      string $title New placeholder text.
 */
function cartography_change_default_title( $title ) {
	$screen = get_current_screen();

	$types = cartography_get_default_labels();

	if ( array_key_exists( $screen->post_type, $types ) ) {
		/* translators: %s represents post type. */
		$title = sprintf( __( 'Enter %s name here', 'cartography' ), strtolower( $types[ $screen->post_type ]['singular'] ) );
	}

	return $title;
}
add_filter( 'enter_title_here', 'cartography_change_default_title' );


/**
 * Updated Messages
 *
 * @since       1.0.0
 * @param       array $messages Post updated message.
 * @return      array $messages New post updated messages.
 */
function cartography_updated_messages( $messages ) {
	global $post, $post_ID;

	$types     = cartography_get_default_labels();
	$open_tag  = '<a href="' . get_permalink( $post_ID ) . '">';
	$close_tag = '</a>';

	foreach ( $types as $type => $labels ) {
		if ( array_key_exists( 'singular', $labels ) ) {
			$messages[ $type ] = array(
				/* translators: Post updated label. %1$s represents anchor open tag, %2$s represents singular label, %3$s represents the anchor closure. */
				1 => sprintf( __( '%2$s updated. %1$sView %2$s%3$s.', 'cartography' ), $open_tag, $labels['singular'], $close_tag ),
				/* translators: Post updated label. %1$s represents anchor open tag, %2$s represents singular label, %3$s represents the anchor closure. */
				4 => sprintf( __( '%2$s updated. %1$sView %2$s%3$s.', 'cartography' ), $open_tag, $labels['singular'], $close_tag ),
				/* translators: Post published label. %1$s represents anchor open tag, %2$s represents singular label, %3$s represents the anchor closure. */
				6 => sprintf( __( '%2$s published. %1$sView %2$s%3$s.', 'cartography' ), $open_tag, $labels['singular'], $close_tag ),
				/* translators: Post saved label. %1$s represents anchor open tag, %2$s represents singular label, %3$s represents the anchor closure. */
				7 => sprintf( __( '%2$s saved. %1$sView %2$s%3$s.', 'cartography' ), $open_tag, $labels['singular'], $close_tag ),
				/* translators: Post submitted label. %1$s represents anchor open tag, %2$s represents singular label, %3$s represents the anchor closure. */
				8 => sprintf( __( '%2$s submitted. %1$sView %2$s%3$s.', 'cartography' ), $open_tag, $labels['singular'], $close_tag ),
			);
		}
	}

	return $messages;
}
add_filter( 'post_updated_messages', 'cartography_updated_messages' );


/**
 * Updated bulk messages
 *
 * @since       1.0.0
 * @param       array $bulk_messages Post updated messages.
 * @param       array $bulk_counts Post counts.
 * @return      array $bulk_messages New post updated messages.
 * @todo        Fix the phpcs ignore lines.
 */
function cartography_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	$types = cartography_get_default_labels();

	foreach ( $types as $type => $labels ) {
		if ( array_key_exists( 'singular', $labels ) && array_key_exists( 'plural', $labels ) ) {
			$bulk_messages[ $type ] = array(
				/* translators: Bulk post updated message. %1$s represents the updated count, %2$s represents singular label, %3$s represents plural label. */
				'updated'   => sprintf( _n( '%1$s %2$s updated.', '%1$s %3$s updated.', $bulk_counts['updated'], 'cartography' ), $bulk_counts['updated'], $label['singular'], $label['plural'] ), // phpcs:ignore
				/* translators: Bulk post locked message. %1$s represents the locked count, %2$s represents singular label, %3$s represents plural label. */
				'locked'    => sprintf( _n( '%1$s %2$s not updated, somebody is editing it.', '%1$s %3$s not updated, somebody is editing them.', $bulk_counts['locked'], 'cartography' ), $bulk_counts['locked'], $label['singular'], $label['plural'] ), // phpcs:ignore
				/* translators: Bulk post deleted message. %1$s represents the deleted count, %2$s represents singular label, %3$s represents plural label. */
				'deleted'   => sprintf( _n( '%1$s %2$s permanently deleted.', '%1$s %3$s permanently deleted.', $bulk_counts['deleted'], 'cartography' ), $bulk_counts['deleted'], $label['singular'], $label['plural'] ), // phpcs:ignore
				/* translators: Bulk post trashed message. %1$s represents the trashed count, %2$s represents singular label, %3$s represents plural label. */
				'trashed'   => sprintf( _n( '%1$s %2$s moved to the Trash.', '%1$s %3$s moved to the Trash.', $bulk_counts['trashed'], 'cartography' ), $bulk_counts['trashed'], $label['singular'], $label['plural'] ), // phpcs:ignore
				/* translators: Bulk post untrashed message. %1$s represents the untrashed count, %2$s represents singular label, %3$s represents plural label. */
				'untrashed' => sprintf( _n( '%1$s %2$s restored from the Trash.', '%1$s %3$s restored from the Trash.', $bulk_counts['untrashed'], 'cartography' ), $bulk_counts['untrashed'], $label['singular'], $label['plural'] ), // phpcs:ignore
			);
		}
	}

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'cartography_bulk_updated_messages', 10, 2 );


/**
 * Registers and sets up the taxonomies for the location CPT
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_setup_taxonomies() {
	$location_singular = cartography_get_label_singular( false, 'location' );

	$category_labels = array(
		/* translators: Taxonomy name. %s represents singular name. */
		'name'              => _x( '%s Categories', 'taxonomy general name', 'cartography' ),
		/* translators: Taxonomy singular name. %s represents singular name. */
		'singular_name'     => _x( '%s Category', 'taxonomy singular name', 'cartography' ),
		/* translators: Taxonomy search label. %s represents singular name. */
		'search_items'      => __( 'Search %s Categories', 'cartography' ),
		/* translators: Taxonomy all items label. %s represents singular name. */
		'all_items'         => __( 'All %s Categories', 'cartography' ),
		/* translators: Taxonomy parent item label. %s represents singular name. */
		'parent_item'       => __( 'Parent %s Category', 'cartography' ),
		/* translators: Taxonomy parent item label with colon. %s represents singular name. */
		'parent_item_colon' => __( 'Parent %s Category:', 'cartography' ),
		/* translators: Taxonomy edit label. %s represents singular name. */
		'edit_item'         => __( 'Edit %s Category', 'cartography' ),
		/* translators: Taxonomy update label. %s represents singular name. */
		'update_item'       => __( 'Update %s Category', 'cartography' ),
		/* translators: Taxonomy add new label. %s represents singular name. */
		'add_new_item'      => __( 'Add New %s Category', 'cartography' ),
		/* translators: Taxonomy new label. %s represents singular name. */
		'new_item_name'     => __( 'New %s Category Name', 'cartography' ),
		/* translators: Taxonomy menu name. %s represents singular name. */
		'menu_name'         => __( '%s Categories', 'cartography' ),
	);

	foreach ( $category_labels as $key => $value ) {
		$category_labels[ $key ] = sprintf( $value, $location_singular );
	}

	$category_args = apply_filters(
		'cartography_location_category_args',
		array(
			'hierarchical' => true,
			'labels'       => apply_filters( 'cartography_location_category_labels', $category_labels ),
			'show_ui'      => true,
			'show_in_menu' => false,
			'query_var'    => 'location_category',
			'rewrite'      => array(
				'slug'         => 'location/category',
				'with_front'   => false,
				'hierarchical' => true,
			),
			'capabilities' => array(
				'manage_terms' => 'manage_location_terms',
				'edit_terms'   => 'edit_location_terms',
				'assign_terms' => 'assign_location_terms',
				'delete_terms' => 'delete_location_terms',
			),
		)
	);
	register_taxonomy( 'location_category', array( 'location' ), $category_args );
	register_taxonomy_for_object_type( 'location_category', 'location' );

	$tag_labels = array(
		/* translators: Taxonomy name. %s represents singular name. */
		'name'                  => _x( '%s Tags', 'taxonomy general name', 'cartography' ),
		/* translators: Taxonomy singular name. %s represents singular name. */
		'singular_name'         => _x( '%s Tag', 'taxonomy singular name', 'cartography' ),
		/* translators: Taxonomy search label. %s represents singular name. */
		'search_items'          => __( 'Search %s Tags', 'cartography' ),
		/* translators: Taxonomy all items label. %s represents singular name. */
		'all_items'             => __( 'All %s Tags', 'cartography' ),
		/* translators: Taxonomy parent item label. %s represents singular name. */
		'parent_item'           => __( 'Parent %s Tag', 'cartography' ),
		/* translators: Taxonomy parent item label with colon. %s represents singular name. */
		'parent_item_colon'     => __( 'Parent %s Tag:', 'cartography' ),
		/* translators: Taxonomy edit label. %s represents singular name. */
		'edit_item'             => __( 'Edit %s Tag', 'cartography' ),
		/* translators: Taxonomy update label. %s represents singular name. */
		'update_item'           => __( 'Update %s Tag', 'cartography' ),
		/* translators: Taxonomy add new label. %s represents singular name. */
		'add_new_item'          => __( 'Add New %s Tag', 'cartography' ),
		/* translators: Taxonomy new label. %s represents singular name. */
		'new_item_name'         => __( 'New %s Tag Name', 'cartography' ),
		'menu_name'             => __( 'Tags', 'cartography' ),
		/* translators: Taxonomy most used label. %s represents singular name. */
		'choose_from_most_used' => sprintf( __( 'Choose from most used %s tags', 'cartography' ), strtolower( $location_singular ) ),
	);

	foreach ( $tag_labels as $key => $value ) {
		$tag_labels[ $key ] = sprintf( $value, $location_singular );
	}

	$tag_args = apply_filters(
		'cartography_location_tag_args',
		array(
			'hierarchical' => false,
			'labels'       => apply_filters( 'cartography_location_tag_labels', $tag_labels ),
			'show_ui'      => true,
			'show_in_menu' => false,
			'query_var'    => 'location_tag',
			'rewrite'      => array(
				'slug'         => 'location/tag',
				'with_front'   => false,
				'hierarchical' => true,
			),
			'capabilities' => array(
				'manage_terms' => 'manage_location_terms',
				'edit_terms'   => 'edit_location_terms',
				'assign_terms' => 'assign_location_terms',
				'delete_terms' => 'delete_location_terms',
			),
		)
	);
	register_taxonomy( 'location_tag', array( 'location' ), $tag_args );
	register_taxonomy_for_object_type( 'location_tag', 'location' );
}
add_action( 'init', 'cartography_setup_taxonomies', 0 );


/**
 * Add admin menu items
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_admin_menu() {
	$location_label = cartography_get_label_singular( false, 'location' );

	/* translators: %s represents post type. */
	$categories_label = sprintf( __( '%s Categories', 'cartography' ), $location_label );

	/* translators: %s represents post type. */
	$tags_label = sprintf( __( '%s Tags', 'cartography' ), $location_label );

	add_submenu_page(
		'edit.php?post_type=map',
		$categories_label,
		$categories_label,
		'edit_location_terms',
		'edit-tags.php?taxonomy=location_category&post_type=location',
	);

	add_submenu_page(
		'edit.php?post_type=map',
		$tags_label,
		$tags_label,
		'edit_location_terms',
		'edit-tags.php?taxonomy=location_tag&post_type=location',
	);
}
add_action( 'admin_menu', 'cartography_admin_menu', 10 );


/**
 * Get the singular and plural labels for a location taxonomy
 *
 * @since       1.0.0
 * @param       string $taxonomy The taxonomy to get labels for.
 * @return      array Associative array of labels (name = plural).
 */
function cartography_get_taxonomy_labels( $taxonomy = 'location_category' ) {
	$allowed_taxonomies = apply_filters(
		'cartography_allowed_location_taxonomies',
		array(
			'location_category',
			'location_tag',
		)
	);

	if ( ! in_array( $taxonomy, $allowed_taxonomies ) ) {
		return false;
	}

	$labels   = array();
	$taxonomy = get_taxonomy( $taxonomy );

	if ( false !== $taxonomy ) {
		$singular  = $taxonomy->labels->singular_name;
		$name      = $taxonomy->labels->name;
		$menu_name = $taxonomy->labels->menu_name;

		$labels = array(
			'name'          => $name,
			'singular_name' => $singular,
			'menu_name'     => $menu_name,
		);
	}

	return apply_filters( 'cartography_get_taxonomy_labels', $labels, $taxonomy );
}


/**
 * Register custom post statuses
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_register_post_statuses() {
	register_post_status(
		'expired',
		array(
			'label'                     => _x( 'Expired', 'Expired map status', 'cartography' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			/* translators: The expired post status label. 1: Singular count label 2: Plural count label. */
			'label_count'               => _n_noop( 'Expired <span class="count">(%s)</span>', 'Expired <span class="count">(%s)</span>', 'cartography' ),
		)
	);
}
add_action( 'init', 'cartography_register_post_statuses', 2 );


/**
 * Sets the parent file for hidden sub menus.
 *
 * @since       1.0.0
 * @param       string $parent_file The menu item parent file.
 * @return      string $parent_file The menu item parent file.
 */
function cartography_parent_file( $parent_file ) {
	global $current_screen;

	if ( 'edit-tags' === $current_screen->base && 'location' === $current_screen->post_type ) {
		$parent_file = 'edit.php?post_type=map';
	}

	return $parent_file;
}
add_filter( 'parent_file', 'cartography_parent_file' );


/**
 * Sets the submenu file for hidden sub menus.
 *
 * @since       1.0.0
 * @param       string $submenu_file The menu item submenu file.
 * @return      string $submenu_file The menu item submenu file.
 */
function cartography_submenu_file( $submenu_file ) {
	global $current_screen;

	if ( 'edit-tags' === $current_screen->base && 'location' === $current_screen->post_type ) {
		$submenu_file = 'edit-tags.php?taxonomy=' . $current_screen->taxonomy . '&post_type=location';
	}

	return $submenu_file;
}
add_filter( 'submenu_file', 'cartography_submenu_file' );
