<?php
/**
 * Country Functions
 *
 * @package     Cartography\Functions\Country
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get states list for a given country.
 *
 * @since       1.0.0
 * @param       string $country The country to get states for.
 * @return      array $states The returned list.
 */
function cartography_get_states( $country = 'US' ) {
	switch ( $country ) {
		case 'US':
			$states = cartography_get_us_states_list();
			break;
		case 'AO':
			$states = cartography_get_angolan_provinces_list();
			break;
		case 'CA':
			$states = cartography_get_canadian_provinces_list();
			break;
		case 'AU':
			$states = cartography_get_australian_states_list();
			break;
		case 'BD':
			$states = cartography_get_bangladeshi_states_list();
			break;
		case 'BG':
			$states = cartography_get_bulgarian_states_list();
			break;
		case 'BR':
			$states = cartography_get_brazilian_states_list();
			break;
		case 'CN':
			$states = cartography_get_chinese_states_list();
			break;
		case 'GB':
			$states = cartography_get_united_kingdom_states_list();
			break;
		case 'HK':
			$states = cartography_get_hong_kong_states_list();
			break;
		case 'HU':
			$states = cartography_get_hungarian_states_list();
			break;
		case 'ID':
			$states = cartography_get_indonesian_states_list();
			break;
		case 'IN':
			$states = cartography_get_indian_states_list();
			break;
		case 'IR':
			$states = cartography_get_iranian_states_list();
			break;
		case 'IT':
			$states = cartography_get_italian_states_list();
			break;
		case 'JP':
			$states = cartography_get_japanese_states_list();
			break;
		case 'MX':
			$states = cartography_get_mexican_states_list();
			break;
		case 'MY':
			$states = cartography_get_malaysian_states_list();
			break;
		case 'NP':
			$states = cartography_get_nepalese_states_list();
			break;
		case 'NZ':
			$states = cartography_get_new_zealand_states_list();
			break;
		case 'PE':
			$states = cartography_get_peruvian_states_list();
			break;
		case 'TH':
			$states = cartography_get_thai_states_list();
			break;
		case 'TR':
			$states = cartography_get_turkey_states_list();
			break;
		case 'ZA':
			$states = cartography_get_south_african_states_list();
			break;
		case 'ES':
			$states = cartography_get_spainish_states_list();
			break;
		default:
			$states = array();
			break;
	}

	return apply_filters( 'cartography_shop_states', $states, $country );
}


/**
 * Get country list
 *
 * @since       1.0.0
 * @return      array $countries A list of the available countries.
 */
function cartography_get_country_list() {
	$countries = array(
		'US' => __( 'United States', 'cartography' ),
		'CA' => __( 'Canada', 'cartography' ),
		'GB' => __( 'United Kingdom', 'cartography' ),
		'AF' => __( 'Afghanistan', 'cartography' ),
		'AX' => __( '&#197;land Islands', 'cartography' ),
		'AL' => __( 'Albania', 'cartography' ),
		'DZ' => __( 'Algeria', 'cartography' ),
		'AS' => __( 'American Samoa', 'cartography' ),
		'AD' => __( 'Andorra', 'cartography' ),
		'AO' => __( 'Angola', 'cartography' ),
		'AI' => __( 'Anguilla', 'cartography' ),
		'AQ' => __( 'Antarctica', 'cartography' ),
		'AG' => __( 'Antigua and Barbuda', 'cartography' ),
		'AR' => __( 'Argentina', 'cartography' ),
		'AM' => __( 'Armenia', 'cartography' ),
		'AW' => __( 'Aruba', 'cartography' ),
		'AU' => __( 'Australia', 'cartography' ),
		'AT' => __( 'Austria', 'cartography' ),
		'AZ' => __( 'Azerbaijan', 'cartography' ),
		'BS' => __( 'Bahamas', 'cartography' ),
		'BH' => __( 'Bahrain', 'cartography' ),
		'BD' => __( 'Bangladesh', 'cartography' ),
		'BB' => __( 'Barbados', 'cartography' ),
		'BY' => __( 'Belarus', 'cartography' ),
		'BE' => __( 'Belgium', 'cartography' ),
		'BZ' => __( 'Belize', 'cartography' ),
		'BJ' => __( 'Benin', 'cartography' ),
		'BM' => __( 'Bermuda', 'cartography' ),
		'BT' => __( 'Bhutan', 'cartography' ),
		'BO' => __( 'Bolivia', 'cartography' ),
		'BQ' => __( 'Bonaire, Saint Eustatius and Saba', 'cartography' ),
		'BA' => __( 'Bosnia and Herzegovina', 'cartography' ),
		'BW' => __( 'Botswana', 'cartography' ),
		'BV' => __( 'Bouvet Island', 'cartography' ),
		'BR' => __( 'Brazil', 'cartography' ),
		'IO' => __( 'British Indian Ocean Territory', 'cartography' ),
		'BN' => __( 'Brunei Darrussalam', 'cartography' ),
		'BG' => __( 'Bulgaria', 'cartography' ),
		'BF' => __( 'Burkina Faso', 'cartography' ),
		'BI' => __( 'Burundi', 'cartography' ),
		'KH' => __( 'Cambodia', 'cartography' ),
		'CM' => __( 'Cameroon', 'cartography' ),
		'CV' => __( 'Cape Verde', 'cartography' ),
		'KY' => __( 'Cayman Islands', 'cartography' ),
		'CF' => __( 'Central African Republic', 'cartography' ),
		'TD' => __( 'Chad', 'cartography' ),
		'CL' => __( 'Chile', 'cartography' ),
		'CN' => __( 'China', 'cartography' ),
		'CX' => __( 'Christmas Island', 'cartography' ),
		'CC' => __( 'Cocos Islands', 'cartography' ),
		'CO' => __( 'Colombia', 'cartography' ),
		'KM' => __( 'Comoros', 'cartography' ),
		'CD' => __( 'Congo, Democratic People\'s Republic', 'cartography' ),
		'CG' => __( 'Congo, Republic of', 'cartography' ),
		'CK' => __( 'Cook Islands', 'cartography' ),
		'CR' => __( 'Costa Rica', 'cartography' ),
		'CI' => __( 'Cote d\'Ivoire', 'cartography' ),
		'HR' => __( 'Croatia/Hrvatska', 'cartography' ),
		'CU' => __( 'Cuba', 'cartography' ),
		'CW' => __( 'Cura&Ccedil;ao', 'cartography' ),
		'CY' => __( 'Cyprus', 'cartography' ),
		'CZ' => __( 'Czechia', 'cartography' ),
		'DK' => __( 'Denmark', 'cartography' ),
		'DJ' => __( 'Djibouti', 'cartography' ),
		'DM' => __( 'Dominica', 'cartography' ),
		'DO' => __( 'Dominican Republic', 'cartography' ),
		'TP' => __( 'East Timor', 'cartography' ),
		'EC' => __( 'Ecuador', 'cartography' ),
		'EG' => __( 'Egypt', 'cartography' ),
		'GQ' => __( 'Equatorial Guinea', 'cartography' ),
		'SV' => __( 'El Salvador', 'cartography' ),
		'ER' => __( 'Eritrea', 'cartography' ),
		'EE' => __( 'Estonia', 'cartography' ),
		'ET' => __( 'Ethiopia', 'cartography' ),
		'FK' => __( 'Falkland Islands', 'cartography' ),
		'FO' => __( 'Faroe Islands', 'cartography' ),
		'FJ' => __( 'Fiji', 'cartography' ),
		'FI' => __( 'Finland', 'cartography' ),
		'FR' => __( 'France', 'cartography' ),
		'GF' => __( 'French Guiana', 'cartography' ),
		'PF' => __( 'French Polynesia', 'cartography' ),
		'TF' => __( 'French Southern Territories', 'cartography' ),
		'GA' => __( 'Gabon', 'cartography' ),
		'GM' => __( 'Gambia', 'cartography' ),
		'GE' => __( 'Georgia', 'cartography' ),
		'DE' => __( 'Germany', 'cartography' ),
		'GR' => __( 'Greece', 'cartography' ),
		'GH' => __( 'Ghana', 'cartography' ),
		'GI' => __( 'Gibraltar', 'cartography' ),
		'GL' => __( 'Greenland', 'cartography' ),
		'GD' => __( 'Grenada', 'cartography' ),
		'GP' => __( 'Guadeloupe', 'cartography' ),
		'GU' => __( 'Guam', 'cartography' ),
		'GT' => __( 'Guatemala', 'cartography' ),
		'GG' => __( 'Guernsey', 'cartography' ),
		'GN' => __( 'Guinea', 'cartography' ),
		'GW' => __( 'Guinea-Bissau', 'cartography' ),
		'GY' => __( 'Guyana', 'cartography' ),
		'HT' => __( 'Haiti', 'cartography' ),
		'HM' => __( 'Heard and McDonald Islands', 'cartography' ),
		'VA' => __( 'Holy See (City Vatican State)', 'cartography' ),
		'HN' => __( 'Honduras', 'cartography' ),
		'HK' => __( 'Hong Kong', 'cartography' ),
		'HU' => __( 'Hungary', 'cartography' ),
		'IS' => __( 'Iceland', 'cartography' ),
		'IN' => __( 'India', 'cartography' ),
		'ID' => __( 'Indonesia', 'cartography' ),
		'IR' => __( 'Iran', 'cartography' ),
		'IQ' => __( 'Iraq', 'cartography' ),
		'IE' => __( 'Ireland', 'cartography' ),
		'IM' => __( 'Isle of Man', 'cartography' ),
		'IL' => __( 'Israel', 'cartography' ),
		'IT' => __( 'Italy', 'cartography' ),
		'JM' => __( 'Jamaica', 'cartography' ),
		'JP' => __( 'Japan', 'cartography' ),
		'JE' => __( 'Jersey', 'cartography' ),
		'JO' => __( 'Jordan', 'cartography' ),
		'KZ' => __( 'Kazakhstan', 'cartography' ),
		'KE' => __( 'Kenya', 'cartography' ),
		'KI' => __( 'Kiribati', 'cartography' ),
		'KW' => __( 'Kuwait', 'cartography' ),
		'KG' => __( 'Kyrgyzstan', 'cartography' ),
		'LA' => __( 'Lao People\'s Democratic Republic', 'cartography' ),
		'LV' => __( 'Latvia', 'cartography' ),
		'LB' => __( 'Lebanon', 'cartography' ),
		'LS' => __( 'Lesotho', 'cartography' ),
		'LR' => __( 'Liberia', 'cartography' ),
		'LY' => __( 'Libyan Arab Jamahiriya', 'cartography' ),
		'LI' => __( 'Liechtenstein', 'cartography' ),
		'LT' => __( 'Lithuania', 'cartography' ),
		'LU' => __( 'Luxembourg', 'cartography' ),
		'MO' => __( 'Macau', 'cartography' ),
		'MK' => __( 'Macedonia', 'cartography' ),
		'MG' => __( 'Madagascar', 'cartography' ),
		'MW' => __( 'Malawi', 'cartography' ),
		'MY' => __( 'Malaysia', 'cartography' ),
		'MV' => __( 'Maldives', 'cartography' ),
		'ML' => __( 'Mali', 'cartography' ),
		'MT' => __( 'Malta', 'cartography' ),
		'MH' => __( 'Marshall Islands', 'cartography' ),
		'MQ' => __( 'Martinique', 'cartography' ),
		'MR' => __( 'Mauritania', 'cartography' ),
		'MU' => __( 'Mauritius', 'cartography' ),
		'YT' => __( 'Mayotte', 'cartography' ),
		'MX' => __( 'Mexico', 'cartography' ),
		'FM' => __( 'Micronesia', 'cartography' ),
		'MD' => __( 'Moldova, Republic of', 'cartography' ),
		'MC' => __( 'Monaco', 'cartography' ),
		'MN' => __( 'Mongolia', 'cartography' ),
		'ME' => __( 'Montenegro', 'cartography' ),
		'MS' => __( 'Montserrat', 'cartography' ),
		'MA' => __( 'Morocco', 'cartography' ),
		'MZ' => __( 'Mozambique', 'cartography' ),
		'MM' => __( 'Myanmar', 'cartography' ),
		'NA' => __( 'Namibia', 'cartography' ),
		'NR' => __( 'Nauru', 'cartography' ),
		'NP' => __( 'Nepal', 'cartography' ),
		'NL' => __( 'Netherlands', 'cartography' ),
		'AN' => __( 'Netherlands Antilles', 'cartography' ),
		'NC' => __( 'New Caledonia', 'cartography' ),
		'NZ' => __( 'New Zealand', 'cartography' ),
		'NI' => __( 'Nicaragua', 'cartography' ),
		'NE' => __( 'Niger', 'cartography' ),
		'NG' => __( 'Nigeria', 'cartography' ),
		'NU' => __( 'Niue', 'cartography' ),
		'NF' => __( 'Norfolk Island', 'cartography' ),
		'KP' => __( 'North Korea', 'cartography' ),
		'MP' => __( 'Northern Mariana Islands', 'cartography' ),
		'NO' => __( 'Norway', 'cartography' ),
		'OM' => __( 'Oman', 'cartography' ),
		'PK' => __( 'Pakistan', 'cartography' ),
		'PW' => __( 'Palau', 'cartography' ),
		'PS' => __( 'Palestinian Territories', 'cartography' ),
		'PA' => __( 'Panama', 'cartography' ),
		'PG' => __( 'Papua New Guinea', 'cartography' ),
		'PY' => __( 'Paraguay', 'cartography' ),
		'PE' => __( 'Peru', 'cartography' ),
		'PH' => __( 'Philippines', 'cartography' ),
		'PN' => __( 'Pitcairn Island', 'cartography' ),
		'PL' => __( 'Poland', 'cartography' ),
		'PT' => __( 'Portugal', 'cartography' ),
		'PR' => __( 'Puerto Rico', 'cartography' ),
		'QA' => __( 'Qatar', 'cartography' ),
		'XK' => __( 'Republic of Kosovo', 'cartography' ),
		'RE' => __( 'Reunion Island', 'cartography' ),
		'RO' => __( 'Romania', 'cartography' ),
		'RU' => __( 'Russian Federation', 'cartography' ),
		'RW' => __( 'Rwanda', 'cartography' ),
		'BL' => __( 'Saint Barth&eacute;lemy', 'cartography' ),
		'SH' => __( 'Saint Helena', 'cartography' ),
		'KN' => __( 'Saint Kitts and Nevis', 'cartography' ),
		'LC' => __( 'Saint Lucia', 'cartography' ),
		'MF' => __( 'Saint Martin (French)', 'cartography' ),
		'SX' => __( 'Saint Martin (Dutch)', 'cartography' ),
		'PM' => __( 'Saint Pierre and Miquelon', 'cartography' ),
		'VC' => __( 'Saint Vincent and the Grenadines', 'cartography' ),
		'SM' => __( 'San Marino', 'cartography' ),
		'ST' => __( 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'cartography' ),
		'SA' => __( 'Saudi Arabia', 'cartography' ),
		'SN' => __( 'Senegal', 'cartography' ),
		'RS' => __( 'Serbia', 'cartography' ),
		'SC' => __( 'Seychelles', 'cartography' ),
		'SL' => __( 'Sierra Leone', 'cartography' ),
		'SG' => __( 'Singapore', 'cartography' ),
		'SK' => __( 'Slovak Republic', 'cartography' ),
		'SI' => __( 'Slovenia', 'cartography' ),
		'SB' => __( 'Solomon Islands', 'cartography' ),
		'SO' => __( 'Somalia', 'cartography' ),
		'ZA' => __( 'South Africa', 'cartography' ),
		'GS' => __( 'South Georgia', 'cartography' ),
		'KR' => __( 'South Korea', 'cartography' ),
		'SS' => __( 'South Sudan', 'cartography' ),
		'ES' => __( 'Spain', 'cartography' ),
		'LK' => __( 'Sri Lanka', 'cartography' ),
		'SD' => __( 'Sudan', 'cartography' ),
		'SR' => __( 'Suriname', 'cartography' ),
		'SJ' => __( 'Svalbard and Jan Mayen Islands', 'cartography' ),
		'SZ' => __( 'Swaziland', 'cartography' ),
		'SE' => __( 'Sweden', 'cartography' ),
		'CH' => __( 'Switzerland', 'cartography' ),
		'SY' => __( 'Syrian Arab Republic', 'cartography' ),
		'TW' => __( 'Taiwan', 'cartography' ),
		'TJ' => __( 'Tajikistan', 'cartography' ),
		'TZ' => __( 'Tanzania', 'cartography' ),
		'TH' => __( 'Thailand', 'cartography' ),
		'TL' => __( 'Timor-Leste', 'cartography' ),
		'TG' => __( 'Togo', 'cartography' ),
		'TK' => __( 'Tokelau', 'cartography' ),
		'TO' => __( 'Tonga', 'cartography' ),
		'TT' => __( 'Trinidad and Tobago', 'cartography' ),
		'TN' => __( 'Tunisia', 'cartography' ),
		'TR' => __( 'Turkey', 'cartography' ),
		'TM' => __( 'Turkmenistan', 'cartography' ),
		'TC' => __( 'Turks and Caicos Islands', 'cartography' ),
		'TV' => __( 'Tuvalu', 'cartography' ),
		'UG' => __( 'Uganda', 'cartography' ),
		'UA' => __( 'Ukraine', 'cartography' ),
		'AE' => __( 'United Arab Emirates', 'cartography' ),
		'UY' => __( 'Uruguay', 'cartography' ),
		'UM' => __( 'US Minor Outlying Islands', 'cartography' ),
		'UZ' => __( 'Uzbekistan', 'cartography' ),
		'VU' => __( 'Vanuatu', 'cartography' ),
		'VE' => __( 'Venezuela', 'cartography' ),
		'VN' => __( 'Vietnam', 'cartography' ),
		'VG' => __( 'Virgin Islands (British)', 'cartography' ),
		'VI' => __( 'Virgin Islands (USA)', 'cartography' ),
		'WF' => __( 'Wallis and Futuna Islands', 'cartography' ),
		'EH' => __( 'Western Sahara', 'cartography' ),
		'WS' => __( 'Western Samoa', 'cartography' ),
		'YE' => __( 'Yemen', 'cartography' ),
		'ZM' => __( 'Zambia', 'cartography' ),
		'ZW' => __( 'Zimbabwe', 'cartography' ),
	);

	return apply_filters( 'cartography_countries', $countries );
}


/**
 * Get US states
 *
 * @since       1.0.0
 * @return      array $states The US states list.
 */
function cartography_get_us_states_list() {
	$states = array(
		''   => '',
		'AL' => __( 'Alabama', 'cartography' ),
		'AK' => __( 'Alaska', 'cartography' ),
		'AZ' => __( 'Arizona', 'cartography' ),
		'AR' => __( 'Arkansas', 'cartography' ),
		'CA' => __( 'California', 'cartography' ),
		'CO' => __( 'Colorado', 'cartography' ),
		'CT' => __( 'Connecticut', 'cartography' ),
		'DE' => __( 'Delaware', 'cartography' ),
		'DC' => __( 'District of Columbia', 'cartography' ),
		'FL' => __( 'Florida', 'cartography' ),
		'GA' => __( 'Georgia', 'cartography' ),
		'HI' => __( 'Hawaii', 'cartography' ),
		'ID' => __( 'Idaho', 'cartography' ),
		'IL' => __( 'Illinois', 'cartography' ),
		'IN' => __( 'Indiana', 'cartography' ),
		'IA' => __( 'Iowa', 'cartography' ),
		'KS' => __( 'Kansas', 'cartography' ),
		'KY' => __( 'Kentucky', 'cartography' ),
		'LA' => __( 'Louisiana', 'cartography' ),
		'ME' => __( 'Maine', 'cartography' ),
		'MD' => __( 'Maryland', 'cartography' ),
		'MA' => __( 'Massachusetts', 'cartography' ),
		'MI' => __( 'Michigan', 'cartography' ),
		'MN' => __( 'Minnesota', 'cartography' ),
		'MS' => __( 'Mississippi', 'cartography' ),
		'MO' => __( 'Missouri', 'cartography' ),
		'MT' => __( 'Montana', 'cartography' ),
		'NE' => __( 'Nebraska', 'cartography' ),
		'NV' => __( 'Nevada', 'cartography' ),
		'NH' => __( 'New Hampshire', 'cartography' ),
		'NJ' => __( 'New Jersey', 'cartography' ),
		'NM' => __( 'New Mexico', 'cartography' ),
		'NY' => __( 'New York', 'cartography' ),
		'NC' => __( 'North Carolina', 'cartography' ),
		'ND' => __( 'North Dakota', 'cartography' ),
		'OH' => __( 'Ohio', 'cartography' ),
		'OK' => __( 'Oklahoma', 'cartography' ),
		'OR' => __( 'Oregon', 'cartography' ),
		'PA' => __( 'Pennsylvania', 'cartography' ),
		'RI' => __( 'Rhode Island', 'cartography' ),
		'SC' => __( 'South Carolina', 'cartography' ),
		'SD' => __( 'South Dakota', 'cartography' ),
		'TN' => __( 'Tennessee', 'cartography' ),
		'TX' => __( 'Texas', 'cartography' ),
		'UT' => __( 'Utah', 'cartography' ),
		'VT' => __( 'Vermont', 'cartography' ),
		'VA' => __( 'Virginia', 'cartography' ),
		'WA' => __( 'Washington', 'cartography' ),
		'WV' => __( 'West Virginia', 'cartography' ),
		'WI' => __( 'Wisconsin', 'cartography' ),
		'WY' => __( 'Wyoming', 'cartography' ),
		'AS' => __( 'American Samoa', 'cartography' ),
		'CZ' => __( 'Canal Zone', 'cartography' ),
		'CM' => __( 'Commonwealth of the Northern Mariana Islands', 'cartography' ),
		'FM' => __( 'Federated States of Micronesia', 'cartography' ),
		'GU' => __( 'Guam', 'cartography' ),
		'MH' => __( 'Marshall Islands', 'cartography' ),
		'MP' => __( 'Northern Mariana Islands', 'cartography' ),
		'PW' => __( 'Palau', 'cartography' ),
		'PI' => __( 'Philippine Islands', 'cartography' ),
		'PR' => __( 'Puerto Rico', 'cartography' ),
		'TT' => __( 'Trust Territory of the Pacific Islands', 'cartography' ),
		'VI' => __( 'Virgin Islands', 'cartography' ),
		'AA' => __( 'Armed Forces - Americas', 'cartography' ),
		'AE' => __( 'Armed Forces - Europe, Canada, Middle East, Africa', 'cartography' ),
		'AP' => __( 'Armed Forces - Pacific', 'cartography' ),
	);

	return apply_filters( 'cartography_us_states', $states );
}


/**
 * Get Angolan provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Angolan provinces.
 */
function cartography_get_angolan_provinces_list() {
	$provinces = array(
		''    => '',
		'BGO' => __( 'Bengo', 'cartography' ),
		'BGU' => __( 'Benguela', 'cartography' ),
		'BIE' => __( 'Bié', 'cartography' ),
		'CAB' => __( 'Cabinda', 'cartography' ),
		'CNN' => __( 'Cunene', 'cartography' ),
		'HUA' => __( 'Huambo', 'cartography' ),
		'HUI' => __( 'Huíla', 'cartography' ),
		'CCU' => __( 'Kuando Kubango', 'cartography' ), // Cuando Cubango.
		'CNO' => __( 'Kwanza-Norte', 'cartography' ), // Cuanza Norte.
		'CUS' => __( 'Kwanza-Sul', 'cartography' ), // Cuanza Sul.
		'LUA' => __( 'Luanda', 'cartography' ),
		'LNO' => __( 'Lunda-Norte', 'cartography' ), // Lunda Norte.
		'LSU' => __( 'Lunda-Sul', 'cartography' ), // Lunda Sul.
		'MAL' => __( 'Malanje', 'cartography' ), // Malanje.
		'MOX' => __( 'Moxico', 'cartography' ),
		'NAM' => __( 'Namibe', 'cartography' ),
		'UIG' => __( 'Uíge', 'cartography' ),
		'ZAI' => __( 'Zaire', 'cartography' ),
	);

	return apply_filters( 'cartography_angolan_provinces', $provinces );
}


/**
 * Get Canadian provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Canadian provinces.
 */
function cartography_get_canadian_provinces_list() {
	$provinces = array(
		''   => '',
		'AB' => __( 'Alberta', 'cartography' ),
		'BC' => __( 'British Columbia', 'cartography' ),
		'MB' => __( 'Manitoba', 'cartography' ),
		'NB' => __( 'New Brunswick', 'cartography' ),
		'NL' => __( 'Newfoundland and Labrador', 'cartography' ),
		'NS' => __( 'Nova Scotia', 'cartography' ),
		'NT' => __( 'Northwest Territories', 'cartography' ),
		'NU' => __( 'Nunavut', 'cartography' ),
		'ON' => __( 'Ontario', 'cartography' ),
		'PE' => __( 'Prince Edward Island', 'cartography' ),
		'QC' => __( 'Quebec', 'cartography' ),
		'SK' => __( 'Saskatchewan', 'cartography' ),
		'YT' => __( 'Yukon', 'cartography' ),
	);

	return apply_filters( 'cartography_canadian_provinces', $provinces );
}


/**
 * Get Australian states
 *
 * @since       1.0.0
 * @return      array $states A list of Australian states.
 */
function cartography_get_australian_states_list() {
	$states = array(
		''    => '',
		'ACT' => __( 'Australian Capital Territory', 'cartography' ),
		'NSW' => __( 'New South Wales', 'cartography' ),
		'NT'  => __( 'Northern Territory', 'cartography' ),
		'QLD' => __( 'Queensland', 'cartography' ),
		'SA'  => __( 'South Australia', 'cartography' ),
		'TAS' => __( 'Tasmania', 'cartography' ),
		'VIC' => __( 'Victoria', 'cartography' ),
		'WA'  => __( 'Western Australia', 'cartography' ),
	);

	return apply_filters( 'cartography_australian_states', $states );
}


/**
 * Get Bangladeshi districts
 *
 * @since       1.0.0
 * @return      array $districts A list of Bangladeshi districts.
 */
function cartography_get_bangladeshi_districts_list() {
	$districts = array(
		''     => '',
		'BAG'  => __( 'Bagerhat', 'cartography' ),
		'BAN'  => __( 'Bandarban', 'cartography' ),
		'BAR'  => __( 'Barguna', 'cartography' ),
		'BARI' => __( 'Barisal', 'cartography' ),
		'BHO'  => __( 'Bhola', 'cartography' ),
		'BOG'  => __( 'Bogra', 'cartography' ),
		'BRA'  => __( 'Brahmanbaria', 'cartography' ),
		'CHA'  => __( 'Chandpur', 'cartography' ),
		'CHI'  => __( 'Chittagong', 'cartography' ),
		'CHU'  => __( 'Chuadanga', 'cartography' ),
		'COM'  => __( 'Comilla', 'cartography' ),
		'COX'  => __( 'Cox\'s Bazar', 'cartography' ),
		'DHA'  => __( 'Dhaka', 'cartography' ),
		'DIN'  => __( 'Dinajpur', 'cartography' ),
		'FAR'  => __( 'Faridpur', 'cartography' ),
		'FEN'  => __( 'Feni', 'cartography' ),
		'GAI'  => __( 'Gaibandha', 'cartography' ),
		'GAZI' => __( 'Gazipur', 'cartography' ),
		'GOP'  => __( 'Gopalganj', 'cartography' ),
		'HAB'  => __( 'Habiganj', 'cartography' ),
		'JAM'  => __( 'Jamalpur', 'cartography' ),
		'JES'  => __( 'Jessore', 'cartography' ),
		'JHA'  => __( 'Jhalokati', 'cartography' ),
		'JHE'  => __( 'Jhenaidah', 'cartography' ),
		'JOY'  => __( 'Joypurhat', 'cartography' ),
		'KHA'  => __( 'Khagrachhari', 'cartography' ),
		'KHU'  => __( 'Khulna', 'cartography' ),
		'KIS'  => __( 'Kishoreganj', 'cartography' ),
		'KUR'  => __( 'Kurigram', 'cartography' ),
		'KUS'  => __( 'Kushtia', 'cartography' ),
		'LAK'  => __( 'Lakshmipur', 'cartography' ),
		'LAL'  => __( 'Lalmonirhat', 'cartography' ),
		'MAD'  => __( 'Madaripur', 'cartography' ),
		'MAG'  => __( 'Magura', 'cartography' ),
		'MAN'  => __( 'Manikganj', 'cartography' ),
		'MEH'  => __( 'Meherpur', 'cartography' ),
		'MOU'  => __( 'Moulvibazar', 'cartography' ),
		'MUN'  => __( 'Munshiganj', 'cartography' ),
		'MYM'  => __( 'Mymensingh', 'cartography' ),
		'NAO'  => __( 'Naogaon', 'cartography' ),
		'NAR'  => __( 'Narail', 'cartography' ),
		'NARG' => __( 'Narayanganj', 'cartography' ),
		'NARD' => __( 'Narsingdi', 'cartography' ),
		'NAT'  => __( 'Natore', 'cartography' ),
		'NAW'  => __( 'Nawabganj', 'cartography' ),
		'NET'  => __( 'Netrakona', 'cartography' ),
		'NIL'  => __( 'Nilphamari', 'cartography' ),
		'NOA'  => __( 'Noakhali', 'cartography' ),
		'PAB'  => __( 'Pabna', 'cartography' ),
		'PAN'  => __( 'Panchagarh', 'cartography' ),
		'PAT'  => __( 'Patuakhali', 'cartography' ),
		'PIR'  => __( 'Pirojpur', 'cartography' ),
		'RAJB' => __( 'Rajbari', 'cartography' ),
		'RAJ'  => __( 'Rajshahi', 'cartography' ),
		'RAN'  => __( 'Rangamati', 'cartography' ),
		'RANP' => __( 'Rangpur', 'cartography' ),
		'SAT'  => __( 'Satkhira', 'cartography' ),
		'SHA'  => __( 'Shariatpur', 'cartography' ),
		'SHE'  => __( 'Sherpur', 'cartography' ),
		'SIR'  => __( 'Sirajganj', 'cartography' ),
		'SUN'  => __( 'Sunamganj', 'cartography' ),
		'SYL'  => __( 'Sylhet', 'cartography' ),
		'TAN'  => __( 'Tangail', 'cartography' ),
		'THA'  => __( 'Thakurgaon', 'cartography' ),
	);

	return apply_filters( 'cartography_bangladeshi_states', $districts );
}


/**
 * Get Brazilian states
 *
 * @since       1.0.0
 * @return      array $states A list of Brazilian states.
 */
function cartography_get_brazilian_states_list() {
	$states = array(
		''   => '',
		'AC' => __( 'Acre', 'cartography' ),
		'AL' => __( 'Alagoas', 'cartography' ),
		'AP' => __( 'Amap&aacute;', 'cartography' ),
		'AM' => __( 'Amazonas', 'cartography' ),
		'BA' => __( 'Bahia', 'cartography' ),
		'CE' => __( 'Cear&aacute;', 'cartography' ),
		'DF' => __( 'Distrito Federal', 'cartography' ),
		'ES' => __( 'Esp&iacute;rito Santo', 'cartography' ),
		'GO' => __( 'Goi&aacute;s', 'cartography' ),
		'MA' => __( 'Maranh&atilde;o', 'cartography' ),
		'MT' => __( 'Mato Grosso', 'cartography' ),
		'MS' => __( 'Mato Grosso do Sul', 'cartography' ),
		'MG' => __( 'Minas Gerais', 'cartography' ),
		'PA' => __( 'Par&aacute;', 'cartography' ),
		'PB' => __( 'Para&iacute;ba', 'cartography' ),
		'PR' => __( 'Paran&aacute;', 'cartography' ),
		'PE' => __( 'Pernambuco', 'cartography' ),
		'PI' => __( 'Piau&iacute;', 'cartography' ),
		'RJ' => __( 'Rio de Janeiro', 'cartography' ),
		'RN' => __( 'Rio Grande do Norte', 'cartography' ),
		'RS' => __( 'Rio Grande do Sul', 'cartography' ),
		'RO' => __( 'Rond&ocirc;nia', 'cartography' ),
		'RR' => __( 'Roraima', 'cartography' ),
		'SC' => __( 'Santa Catarina', 'cartography' ),
		'SP' => __( 'S&atilde;o Paulo', 'cartography' ),
		'SE' => __( 'Sergipe', 'cartography' ),
		'TO' => __( 'Tocantins', 'cartography' ),
	);

	return apply_filters( 'cartography_brazilian_states', $states );
}


/**
 * Get Bulgarian provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Bulgarian provinces.
 */
function cartography_get_bulgarian_provinces_list() {
	$provinces = array(
		''      => '',
		'BG-01' => __( 'Blagoevgrad', 'cartography' ),
		'BG-02' => __( 'Burgas', 'cartography' ),
		'BG-08' => __( 'Dobrich', 'cartography' ),
		'BG-07' => __( 'Gabrovo', 'cartography' ),
		'BG-26' => __( 'Haskovo', 'cartography' ),
		'BG-09' => __( 'Kardzhali', 'cartography' ),
		'BG-10' => __( 'Kyustendil', 'cartography' ),
		'BG-11' => __( 'Lovech', 'cartography' ),
		'BG-12' => __( 'Montana', 'cartography' ),
		'BG-13' => __( 'Pazardzhik', 'cartography' ),
		'BG-14' => __( 'Pernik', 'cartography' ),
		'BG-15' => __( 'Pleven', 'cartography' ),
		'BG-16' => __( 'Plovdiv', 'cartography' ),
		'BG-17' => __( 'Razgrad', 'cartography' ),
		'BG-18' => __( 'Ruse', 'cartography' ),
		'BG-27' => __( 'Shumen', 'cartography' ),
		'BG-19' => __( 'Silistra', 'cartography' ),
		'BG-20' => __( 'Sliven', 'cartography' ),
		'BG-21' => __( 'Smolyan', 'cartography' ),
		'BG-23' => __( 'Sofia', 'cartography' ),
		'BG-22' => __( 'Sofia-Grad', 'cartography' ),
		'BG-24' => __( 'Stara Zagora', 'cartography' ),
		'BG-25' => __( 'Targovishte', 'cartography' ),
		'BG-03' => __( 'Varna', 'cartography' ),
		'BG-04' => __( 'Veliko Tarnovo', 'cartography' ),
		'BG-05' => __( 'Vidin', 'cartography' ),
		'BG-06' => __( 'Vratsa', 'cartography' ),
		'BG-28' => __( 'Yambol', 'cartography' ),
	);

	return apply_filters( 'cartography_bulgarian_pronvinces', $provinces );
}


/**
 * Get Hong Kong states
 *
 * @since       1.0.0
 * @return      array $states A list of Hong Kong states.
 */
function cartography_get_hong_kong_states_list() {
	$states = array(
		''                => '',
		'HONG KONG'       => __( 'Hong Kong Island', 'cartography' ),
		'KOWLOON'         => __( 'Kowloon', 'cartography' ),
		'NEW TERRITORIES' => __( 'New Territories', 'cartography' ),
	);

	return apply_filters( 'cartography_hong_kong_states', $states );
}


/**
 * Get Hungary counties
 *
 * @since       1.0.0
 * @return      array $counties A list of Hungarian counties.
 */
function cartography_get_hungarian_counties_list() {
	$counties = array(
		''   => '',
		'BK' => __( 'Bács-Kiskun', 'cartography' ),
		'BE' => __( 'Békés', 'cartography' ),
		'BA' => __( 'Baranya', 'cartography' ),
		'BZ' => __( 'Borsod-Abaúj-Zemplén', 'cartography' ),
		'BU' => __( 'Budapest', 'cartography' ),
		'CS' => __( 'Csongrád', 'cartography' ),
		'FE' => __( 'Fejér', 'cartography' ),
		'GS' => __( 'Győr-Moson-Sopron', 'cartography' ),
		'HB' => __( 'Hajdú-Bihar', 'cartography' ),
		'HE' => __( 'Heves', 'cartography' ),
		'JN' => __( 'Jász-Nagykun-Szolnok', 'cartography' ),
		'KE' => __( 'Komárom-Esztergom', 'cartography' ),
		'NO' => __( 'Nógrád', 'cartography' ),
		'PE' => __( 'Pest', 'cartography' ),
		'SO' => __( 'Somogy', 'cartography' ),
		'SZ' => __( 'Szabolcs-Szatmár-Bereg', 'cartography' ),
		'TO' => __( 'Tolna', 'cartography' ),
		'VA' => __( 'Vas', 'cartography' ),
		'VE' => __( 'Veszprém', 'cartography' ),
		'ZA' => __( 'Zala', 'cartography' ),
	);

	return apply_filters( 'cartography_hungarian_counties', $counties );
}


/**
 * Get Japanese prefectures
 *
 * @since       1.0.0
 * @return      array $prefectures A list of Japanese prefectures.
 */
function cartography_get_japanese_prefectures_list() {
	$prefectures = array(
		''     => '',
		'JP01' => __( 'Hokkaido', 'cartography' ),
		'JP02' => __( 'Aomori', 'cartography' ),
		'JP03' => __( 'Iwate', 'cartography' ),
		'JP04' => __( 'Miyagi', 'cartography' ),
		'JP05' => __( 'Akita', 'cartography' ),
		'JP06' => __( 'Yamagata', 'cartography' ),
		'JP07' => __( 'Fukushima', 'cartography' ),
		'JP08' => __( 'Ibaraki', 'cartography' ),
		'JP09' => __( 'Tochigi', 'cartography' ),
		'JP10' => __( 'Gunma', 'cartography' ),
		'JP11' => __( 'Saitama', 'cartography' ),
		'JP12' => __( 'Chiba', 'cartography' ),
		'JP13' => __( 'Tokyo', 'cartography' ),
		'JP14' => __( 'Kanagawa', 'cartography' ),
		'JP15' => __( 'Niigata', 'cartography' ),
		'JP16' => __( 'Toyama', 'cartography' ),
		'JP17' => __( 'Ishikawa', 'cartography' ),
		'JP18' => __( 'Fukui', 'cartography' ),
		'JP19' => __( 'Yamanashi', 'cartography' ),
		'JP20' => __( 'Nagano', 'cartography' ),
		'JP21' => __( 'Gifu', 'cartography' ),
		'JP22' => __( 'Shizuoka', 'cartography' ),
		'JP23' => __( 'Aichi', 'cartography' ),
		'JP24' => __( 'Mie', 'cartography' ),
		'JP25' => __( 'Shiga', 'cartography' ),
		'JP26' => __( 'Kyouto', 'cartography' ),
		'JP27' => __( 'Osaka', 'cartography' ),
		'JP28' => __( 'Hyougo', 'cartography' ),
		'JP29' => __( 'Nara', 'cartography' ),
		'JP30' => __( 'Wakayama', 'cartography' ),
		'JP31' => __( 'Tottori', 'cartography' ),
		'JP32' => __( 'Shimane', 'cartography' ),
		'JP33' => __( 'Okayama', 'cartography' ),
		'JP34' => __( 'Hiroshima', 'cartography' ),
		'JP35' => __( 'Yamaguchi', 'cartography' ),
		'JP36' => __( 'Tokushima', 'cartography' ),
		'JP37' => __( 'Kagawa', 'cartography' ),
		'JP38' => __( 'Ehime', 'cartography' ),
		'JP39' => __( 'Kochi', 'cartography' ),
		'JP40' => __( 'Fukuoka', 'cartography' ),
		'JP41' => __( 'Saga', 'cartography' ),
		'JP42' => __( 'Nagasaki', 'cartography' ),
		'JP43' => __( 'Kumamoto', 'cartography' ),
		'JP44' => __( 'Oita', 'cartography' ),
		'JP45' => __( 'Miyazaki', 'cartography' ),
		'JP46' => __( 'Kagoshima', 'cartography' ),
		'JP47' => __( 'Okinawa', 'cartography' ),
	);

	return apply_filters( 'cartography_japanese_prefectures', $prefectures );
}


/**
 * Get Chinese provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Chinese provinces
 */
function cartography_get_chinese_provinces_list() {
	$provinces = array(
		''     => '',
		'CN1'  => __( 'Yunnan / &#20113;&#21335;', 'cartography' ),
		'CN2'  => __( 'Beijing / &#21271;&#20140;', 'cartography' ),
		'CN3'  => __( 'Tianjin / &#22825;&#27941;', 'cartography' ),
		'CN4'  => __( 'Hebei / &#27827;&#21271;', 'cartography' ),
		'CN5'  => __( 'Shanxi / &#23665;&#35199;', 'cartography' ),
		'CN6'  => __( 'Inner Mongolia / &#20839;&#33945;&#21476;', 'cartography' ),
		'CN7'  => __( 'Liaoning / &#36797;&#23425;', 'cartography' ),
		'CN8'  => __( 'Jilin / &#21513;&#26519;', 'cartography' ),
		'CN9'  => __( 'Heilongjiang / &#40657;&#40857;&#27743;', 'cartography' ),
		'CN10' => __( 'Shanghai / &#19978;&#28023;', 'cartography' ),
		'CN11' => __( 'Jiangsu / &#27743;&#33487;', 'cartography' ),
		'CN12' => __( 'Zhejiang / &#27993;&#27743;', 'cartography' ),
		'CN13' => __( 'Anhui / &#23433;&#24509;', 'cartography' ),
		'CN14' => __( 'Fujian / &#31119;&#24314;', 'cartography' ),
		'CN15' => __( 'Jiangxi / &#27743;&#35199;', 'cartography' ),
		'CN16' => __( 'Shandong / &#23665;&#19996;', 'cartography' ),
		'CN17' => __( 'Henan / &#27827;&#21335;', 'cartography' ),
		'CN18' => __( 'Hubei / &#28246;&#21271;', 'cartography' ),
		'CN19' => __( 'Hunan / &#28246;&#21335;', 'cartography' ),
		'CN20' => __( 'Guangdong / &#24191;&#19996;', 'cartography' ),
		'CN21' => __( 'Guangxi Zhuang / &#24191;&#35199;&#22766;&#26063;', 'cartography' ),
		'CN22' => __( 'Hainan / &#28023;&#21335;', 'cartography' ),
		'CN23' => __( 'Chongqing / &#37325;&#24198;', 'cartography' ),
		'CN24' => __( 'Sichuan / &#22235;&#24029;', 'cartography' ),
		'CN25' => __( 'Guizhou / &#36149;&#24030;', 'cartography' ),
		'CN26' => __( 'Shaanxi / &#38485;&#35199;', 'cartography' ),
		'CN27' => __( 'Gansu / &#29976;&#32899;', 'cartography' ),
		'CN28' => __( 'Qinghai / &#38738;&#28023;', 'cartography' ),
		'CN29' => __( 'Ningxia Hui / &#23425;&#22799;', 'cartography' ),
		'CN30' => __( 'Macau / &#28595;&#38376;', 'cartography' ),
		'CN31' => __( 'Tibet / &#35199;&#34255;', 'cartography' ),
		'CN32' => __( 'Xinjiang / &#26032;&#30086;', 'cartography' ),
	);

	return apply_filters( 'cartography_chinese_pronvinces', $provinces );
}


/**
 * Get United Kingdom counties
 *
 * @since       1.0.0
 * @return      array $counties A list of UK counties
 */
function cartography_get_united_kingdom_counties_list() {
	$counties = array(
		''       => '',
		'GB-ABE' => __( 'Aberdeen City', 'cartography' ),
		'GB-ABD' => __( 'Aberdeenshire', 'cartography' ),
		'GB-ANS' => __( 'Angus', 'cartography' ),
		'GB-ANN' => __( 'Antrim and Newtownabbey', 'cartography' ),
		'GB-AND' => __( 'Ards and North Down', 'cartography' ),
		'GB-AGB' => __( 'Argyll and Bute', 'cartography' ),
		'GB-ABC' => __( 'Armagh, Banbridge and Craigavon', 'cartography' ),
		'GB-BDG' => __( 'Barking and Dagenham', 'cartography' ),
		'GB-BNE' => __( 'Barnet', 'cartography' ),
		'GB-BNS' => __( 'Barnsley', 'cartography' ),
		'GB-BAS' => __( 'Bath and North East Somerset', 'cartography' ),
		'GB-BDF' => __( 'Bedford', 'cartography' ),
		'GB-BFS' => __( 'Belfast', 'cartography' ),
		'GB-BEX' => __( 'Bexley', 'cartography' ),
		'GB-BIR' => __( 'Birmingham', 'cartography' ),
		'GB-BBD' => __( 'Blackburn with Darwen', 'cartography' ),
		'GB-BPL' => __( 'Blackpool', 'cartography' ),
		'GB-BGW' => __( 'Blaenau Gwent', 'cartography' ),
		'GB-BOL' => __( 'Bolton', 'cartography' ),
		'GB-BMH' => __( 'Bournemouth', 'cartography' ),
		'GB-BRC' => __( 'Bracknell Forest', 'cartography' ),
		'GB-BRD' => __( 'Bradford', 'cartography' ),
		'GB-BEN' => __( 'Brent', 'cartography' ),
		'GB-BGE' => __( 'Bridgend', 'cartography' ),
		'GB-BNH' => __( 'Brighton and Hove', 'cartography' ),
		'GB-BST' => __( 'Bristol, City of', 'cartography' ),
		'GB-BRY' => __( 'Bromley', 'cartography' ),
		'GB-BKM' => __( 'Buckinghamshire', 'cartography' ),
		'GB-BUR' => __( 'Bury', 'cartography' ),
		'GB-CAY' => __( 'Caerphilly', 'cartography' ),
		'GB-CLD' => __( 'Calderdale', 'cartography' ),
		'GB-CAM' => __( 'Cambridgeshire', 'cartography' ),
		'GB-CMD' => __( 'Camden', 'cartography' ),
		'GB-CRF' => __( 'Cardiff', 'cartography' ),
		'GB-CMN' => __( 'Carmarthenshire', 'cartography' ),
		'GB-CCG' => __( 'Causeway Coast and Glens', 'cartography' ),
		'GB-CBF' => __( 'Central Bedfordshire', 'cartography' ),
		'GB-CGN' => __( 'Ceredigion', 'cartography' ),
		'GB-CHE' => __( 'Cheshire East', 'cartography' ),
		'GB-CHW' => __( 'Cheshire West and Chester', 'cartography' ),
		'GB-CLK' => __( 'Clackmannanshire', 'cartography' ),
		'GB-CWY' => __( 'Conwy', 'cartography' ),
		'GB-CON' => __( 'Cornwall', 'cartography' ),
		'GB-COV' => __( 'Coventry', 'cartography' ),
		'GB-CRY' => __( 'Croydon', 'cartography' ),
		'GB-CMA' => __( 'Cumbria', 'cartography' ),
		'GB-DAL' => __( 'Darlington', 'cartography' ),
		'GB-DEN' => __( 'Denbighshire', 'cartography' ),
		'GB-DER' => __( 'Derby', 'cartography' ),
		'GB-DBY' => __( 'Derbyshire', 'cartography' ),
		'GB-DRS' => __( 'Derry and Strabane', 'cartography' ),
		'GB-DEV' => __( 'Devon', 'cartography' ),
		'GB-DNC' => __( 'Doncaster', 'cartography' ),
		'GB-DOR' => __( 'Dorset', 'cartography' ),
		'GB-DUD' => __( 'Dudley', 'cartography' ),
		'GB-DGY' => __( 'Dumfries and Galloway', 'cartography' ),
		'GB-DND' => __( 'Dundee City', 'cartography' ),
		'GB-DUR' => __( 'Durham, County', 'cartography' ),
		'GB-EAL' => __( 'Ealing', 'cartography' ),
		'GB-EAY' => __( 'East Ayrshire', 'cartography' ),
		'GB-EDU' => __( 'East Dunbartonshire', 'cartography' ),
		'GB-ELN' => __( 'East Lothian', 'cartography' ),
		'GB-ERW' => __( 'East Renfrewshire', 'cartography' ),
		'GB-ERY' => __( 'East Riding of Yorkshire', 'cartography' ),
		'GB-ESX' => __( 'East Sussex', 'cartography' ),
		'GB-EDH' => __( 'Edinburgh, City of', 'cartography' ),
		'GB-ELS' => __( 'Eilean Siar', 'cartography' ),
		'GB-ENF' => __( 'Enfield', 'cartography' ),
		'GB-ESS' => __( 'Essex', 'cartography' ),
		'GB-FAL' => __( 'Falkirk', 'cartography' ),
		'GB-FMO' => __( 'Fermanagh and Omagh', 'cartography' ),
		'GB-FIF' => __( 'Fife', 'cartography' ),
		'GB-FLN' => __( 'Flintshire', 'cartography' ),
		'GB-GAT' => __( 'Gateshead', 'cartography' ),
		'GB-GLG' => __( 'Glasgow City', 'cartography' ),
		'GB-GLS' => __( 'Gloucestershire', 'cartography' ),
		'GB-GRE' => __( 'Greenwich', 'cartography' ),
		'GB-GWN' => __( 'Gwynedd', 'cartography' ),
		'GB-HCK' => __( 'Hackney', 'cartography' ),
		'GB-HAL' => __( 'Halton', 'cartography' ),
		'GB-HMF' => __( 'Hammersmith and Fulham', 'cartography' ),
		'GB-HAM' => __( 'Hampshire', 'cartography' ),
		'GB-HRY' => __( 'Haringey', 'cartography' ),
		'GB-HRW' => __( 'Harrow', 'cartography' ),
		'GB-HPL' => __( 'Hartlepool', 'cartography' ),
		'GB-HAV' => __( 'Havering', 'cartography' ),
		'GB-HEF' => __( 'Herefordshire', 'cartography' ),
		'GB-HRT' => __( 'Hertfordshire', 'cartography' ),
		'GB-HLD' => __( 'Highland', 'cartography' ),
		'GB-HIL' => __( 'Hillingdon', 'cartography' ),
		'GB-HNS' => __( 'Hounslow', 'cartography' ),
		'GB-IVC' => __( 'Inverclyde', 'cartography' ),
		'GB-AGY' => __( 'Isle of Anglesey', 'cartography' ),
		'GB-IOW' => __( 'Isle of Wight', 'cartography' ),
		'GB-IOS' => __( 'Isles of Scilly', 'cartography' ),
		'GB-ISL' => __( 'Islington', 'cartography' ),
		'GB-KEC' => __( 'Kensington and Chelsea', 'cartography' ),
		'GB-KEN' => __( 'Kent', 'cartography' ),
		'GB-KHL' => __( 'Kingston upon Hull', 'cartography' ),
		'GB-KTT' => __( 'Kingston upon Thames', 'cartography' ),
		'GB-KIR' => __( 'Kirklees', 'cartography' ),
		'GB-KWL' => __( 'Knowsley', 'cartography' ),
		'GB-LBH' => __( 'Lambeth', 'cartography' ),
		'GB-LAN' => __( 'Lancashire', 'cartography' ),
		'GB-LDS' => __( 'Leeds', 'cartography' ),
		'GB-LCE' => __( 'Leicester', 'cartography' ),
		'GB-LEC' => __( 'Leicestershire', 'cartography' ),
		'GB-LEW' => __( 'Lewisham', 'cartography' ),
		'GB-LIN' => __( 'Lincolnshire', 'cartography' ),
		'GB-LBC' => __( 'Lisburn and Castlereagh', 'cartography' ),
		'GB-LIV' => __( 'Liverpool', 'cartography' ),
		'GB-LND' => __( 'London, City of', 'cartography' ),
		'GB-LUT' => __( 'Luton', 'cartography' ),
		'GB-MAN' => __( 'Manchester', 'cartography' ),
		'GB-MDW' => __( 'Medway', 'cartography' ),
		'GB-MTY' => __( 'Merthyr Tydfil', 'cartography' ),
		'GB-MRT' => __( 'Merton', 'cartography' ),
		'GB-MEA' => __( 'Mid and East Antrim', 'cartography' ),
		'GB-MUL' => __( 'Mid Ulster', 'cartography' ),
		'GB-MDB' => __( 'Middlesbrough', 'cartography' ),
		'GB-MLN' => __( 'Midlothian', 'cartography' ),
		'GB-MIK' => __( 'Milton Keynes', 'cartography' ),
		'GB-MON' => __( 'Monmouthshire', 'cartography' ),
		'GB-MRY' => __( 'Moray', 'cartography' ),
		'GB-NTL' => __( 'Neath Port Talbot', 'cartography' ),
		'GB-NET' => __( 'Newcastle upon Tyne', 'cartography' ),
		'GB-NWM' => __( 'Newham', 'cartography' ),
		'GB-NWP' => __( 'Newport', 'cartography' ),
		'GB-NMD' => __( 'Newry, Mourne and Down', 'cartography' ),
		'GB-NFK' => __( 'Norfolk', 'cartography' ),
		'GB-NAY' => __( 'North Ayrshire', 'cartography' ),
		'GB-NEL' => __( 'North East Lincolnshire', 'cartography' ),
		'GB-NLK' => __( 'North Lanarkshire', 'cartography' ),
		'GB-NLN' => __( 'North Lincolnshire', 'cartography' ),
		'GB-NSM' => __( 'North Somerset', 'cartography' ),
		'GB-NTY' => __( 'North Tyneside', 'cartography' ),
		'GB-NYK' => __( 'North Yorkshire', 'cartography' ),
		'GB-NTH' => __( 'Northamptonshire', 'cartography' ),
		'GB-NBL' => __( 'Northumberland', 'cartography' ),
		'GB-NGM' => __( 'Nottingham', 'cartography' ),
		'GB-NTT' => __( 'Nottinghamshire', 'cartography' ),
		'GB-OLD' => __( 'Oldham', 'cartography' ),
		'GB-ORK' => __( 'Orkney Islands', 'cartography' ),
		'GB-OXF' => __( 'Oxfordshire', 'cartography' ),
		'GB-PEM' => __( 'Pembrokeshire', 'cartography' ),
		'GB-PKN' => __( 'Perth and Kinross', 'cartography' ),
		'GB-PTE' => __( 'Peterborough', 'cartography' ),
		'GB-PLY' => __( 'Plymouth', 'cartography' ),
		'GB-POL' => __( 'Poole', 'cartography' ),
		'GB-POR' => __( 'Portsmouth', 'cartography' ),
		'GB-POW' => __( 'Powys', 'cartography' ),
		'GB-RDG' => __( 'Reading', 'cartography' ),
		'GB-RDB' => __( 'Redbridge', 'cartography' ),
		'GB-RCC' => __( 'Redcar and Cleveland', 'cartography' ),
		'GB-RFW' => __( 'Renfrewshire', 'cartography' ),
		'GB-RCT' => __( 'Rhondda, Cynon, Taff', 'cartography' ),
		'GB-RIC' => __( 'Richmond upon Thames', 'cartography' ),
		'GB-RCH' => __( 'Rochdale', 'cartography' ),
		'GB-ROT' => __( 'Rotherham', 'cartography' ),
		'GB-RUT' => __( 'Rutland', 'cartography' ),
		'GB-SLF' => __( 'Salford', 'cartography' ),
		'GB-SAW' => __( 'Sandwell', 'cartography' ),
		'GB-SCB' => __( 'Scottish Borders, The', 'cartography' ),
		'GB-SFT' => __( 'Sefton', 'cartography' ),
		'GB-SHF' => __( 'Sheffield', 'cartography' ),
		'GB-ZET' => __( 'Shetland Islands', 'cartography' ),
		'GB-SHR' => __( 'Shropshire', 'cartography' ),
		'GB-SLG' => __( 'Slough', 'cartography' ),
		'GB-SOL' => __( 'Solihull', 'cartography' ),
		'GB-SOM' => __( 'Somerset', 'cartography' ),
		'GB-SAY' => __( 'South Ayrshire', 'cartography' ),
		'GB-SGC' => __( 'South Gloucestershire', 'cartography' ),
		'GB-SLK' => __( 'South Lanarkshire', 'cartography' ),
		'GB-STY' => __( 'South Tyneside', 'cartography' ),
		'GB-STH' => __( 'Southampton', 'cartography' ),
		'GB-SOS' => __( 'Southend-on-Sea', 'cartography' ),
		'GB-SWK' => __( 'Southwark', 'cartography' ),
		'GB-SHN' => __( 'St. Helens', 'cartography' ),
		'GB-STS' => __( 'Staffordshire', 'cartography' ),
		'GB-STG' => __( 'Stirling', 'cartography' ),
		'GB-SKP' => __( 'Stockport', 'cartography' ),
		'GB-STT' => __( 'Stockton-on-Tees', 'cartography' ),
		'GB-STE' => __( 'Stoke-on-Trent', 'cartography' ),
		'GB-SFK' => __( 'Suffolk', 'cartography' ),
		'GB-SND' => __( 'Sunderland', 'cartography' ),
		'GB-SRY' => __( 'Surrey', 'cartography' ),
		'GB-STN' => __( 'Sutton', 'cartography' ),
		'GB-SWA' => __( 'Swansea', 'cartography' ),
		'GB-SWD' => __( 'Swindon', 'cartography' ),
		'GB-TAM' => __( 'Tameside', 'cartography' ),
		'GB-TFW' => __( 'Telford and Wrekin', 'cartography' ),
		'GB-THR' => __( 'Thurrock', 'cartography' ),
		'GB-TOB' => __( 'Torbay', 'cartography' ),
		'GB-TOF' => __( 'Torfaen', 'cartography' ),
		'GB-TWH' => __( 'Tower Hamlets', 'cartography' ),
		'GB-TRF' => __( 'Trafford', 'cartography' ),
		'GB-VGL' => __( 'Vale of Glamorgan, The', 'cartography' ),
		'GB-WKF' => __( 'Wakefield', 'cartography' ),
		'GB-WLL' => __( 'Walsall', 'cartography' ),
		'GB-WFT' => __( 'Waltham Forest', 'cartography' ),
		'GB-WND' => __( 'Wandsworth', 'cartography' ),
		'GB-WRT' => __( 'Warrington', 'cartography' ),
		'GB-WAR' => __( 'Warwickshire', 'cartography' ),
		'GB-WBK' => __( 'West Berkshire', 'cartography' ),
		'GB-WDU' => __( 'West Dunbartonshire', 'cartography' ),
		'GB-WLN' => __( 'West Lothian', 'cartography' ),
		'GB-WSX' => __( 'West Sussex', 'cartography' ),
		'GB-WSM' => __( 'Westminster', 'cartography' ),
		'GB-WGN' => __( 'Wigan', 'cartography' ),
		'GB-WIL' => __( 'Wiltshire', 'cartography' ),
		'GB-WNM' => __( 'Windsor and Maidenhead', 'cartography' ),
		'GB-WRL' => __( 'Wirral', 'cartography' ),
		'GB-WOK' => __( 'Wokingham', 'cartography' ),
		'GB-WLV' => __( 'Wolverhampton', 'cartography' ),
		'GB-WOR' => __( 'Worcestershire', 'cartography' ),
		'GB-WRX' => __( 'Wrexham', 'cartography' ),
		'GB-YOR' => __( 'York', 'cartography' ),
	);

	return apply_filters( 'cartography_united_kingdom_counties', $counties );
}


/**
 * Get New Zealand provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of provinces
 */
function cartography_get_new_zealand_regions_list() {
	$provinces = array(
		''   => '',
		'AK' => __( 'Auckland', 'cartography' ),
		'BP' => __( 'Bay of Plenty', 'cartography' ),
		'CT' => __( 'Canterbury', 'cartography' ),
		'HB' => __( 'Hawke&rsquo;s Bay', 'cartography' ),
		'MW' => __( 'Manawatu-Wanganui', 'cartography' ),
		'MB' => __( 'Marlborough', 'cartography' ),
		'NS' => __( 'Nelson', 'cartography' ),
		'NL' => __( 'Northland', 'cartography' ),
		'OT' => __( 'Otago', 'cartography' ),
		'SL' => __( 'Southland', 'cartography' ),
		'TK' => __( 'Taranaki', 'cartography' ),
		'TM' => __( 'Tasman', 'cartography' ),
		'WA' => __( 'Waikato', 'cartography' ),
		'WR' => __( 'Wairarapa', 'cartography' ),
		'WE' => __( 'Wellington', 'cartography' ),
		'WC' => __( 'West Coast', 'cartography' ),
	);

	return apply_filters( 'cartography_new_zealand_provices', $provinces );
}


/**
 * Get Peruvian departments
 *
 * @since       1.0.0
 * @return      array $departments A list of Peruvian departments.
 */
function cartography_get_peruvian_departments_list() {
	$departments = array(
		''    => '',
		'CAL' => __( 'El Callao', 'cartography' ),
		'LMA' => __( 'Municipalidad Metropolitana de Lima', 'cartography' ),
		'AMA' => __( 'Amazonas', 'cartography' ),
		'ANC' => __( 'Ancash', 'cartography' ),
		'APU' => __( 'Apur&iacute;mac', 'cartography' ),
		'ARE' => __( 'Arequipa', 'cartography' ),
		'AYA' => __( 'Ayacucho', 'cartography' ),
		'CAJ' => __( 'Cajamarca', 'cartography' ),
		'CUS' => __( 'Cusco', 'cartography' ),
		'HUV' => __( 'Huancavelica', 'cartography' ),
		'HUC' => __( 'Hu&aacute;nuco', 'cartography' ),
		'ICA' => __( 'Ica', 'cartography' ),
		'JUN' => __( 'Jun&iacute;n', 'cartography' ),
		'LAL' => __( 'La Libertad', 'cartography' ),
		'LAM' => __( 'Lambayeque', 'cartography' ),
		'LIM' => __( 'Lima', 'cartography' ),
		'LOR' => __( 'Loreto', 'cartography' ),
		'MDD' => __( 'Madre de Dios', 'cartography' ),
		'MOQ' => __( 'Moquegua', 'cartography' ),
		'PAS' => __( 'Pasco', 'cartography' ),
		'PIU' => __( 'Piura', 'cartography' ),
		'PUN' => __( 'Puno', 'cartography' ),
		'SAM' => __( 'San Mart&iacute;n', 'cartography' ),
		'TAC' => __( 'Tacna', 'cartography' ),
		'TUM' => __( 'Tumbes', 'cartography' ),
		'UCA' => __( 'Ucayali', 'cartography' ),
	);

	return apply_filters( 'cartography_peruvian_departments', $departments );
}


/**
 * Get Indonesian provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Indonesian provinces.
 */
function cartography_get_indonesian_provinces_list() {
	$provinces = array(
		''   => '',
		'AC' => __( 'Daerah Istimewa Aceh', 'cartography' ),
		'SU' => __( 'Sumatera Utara', 'cartography' ),
		'SB' => __( 'Sumatera Barat', 'cartography' ),
		'RI' => __( 'Riau', 'cartography' ),
		'KR' => __( 'Kepulauan Riau', 'cartography' ),
		'JA' => __( 'Jambi', 'cartography' ),
		'SS' => __( 'Sumatera Selatan', 'cartography' ),
		'BB' => __( 'Bangka Belitung', 'cartography' ),
		'BE' => __( 'Bengkulu', 'cartography' ),
		'LA' => __( 'Lampung', 'cartography' ),
		'JK' => __( 'DKI Jakarta', 'cartography' ),
		'JB' => __( 'Jawa Barat', 'cartography' ),
		'BT' => __( 'Banten', 'cartography' ),
		'JT' => __( 'Jawa Tengah', 'cartography' ),
		'JI' => __( 'Jawa Timur', 'cartography' ),
		'YO' => __( 'Daerah Istimewa Yogyakarta', 'cartography' ),
		'BA' => __( 'Bali', 'cartography' ),
		'NB' => __( 'Nusa Tenggara Barat', 'cartography' ),
		'NT' => __( 'Nusa Tenggara Timur', 'cartography' ),
		'KB' => __( 'Kalimantan Barat', 'cartography' ),
		'KT' => __( 'Kalimantan Tengah', 'cartography' ),
		'KI' => __( 'Kalimantan Timur', 'cartography' ),
		'KS' => __( 'Kalimantan Selatan', 'cartography' ),
		'KU' => __( 'Kalimantan Utara', 'cartography' ),
		'SA' => __( 'Sulawesi Utara', 'cartography' ),
		'ST' => __( 'Sulawesi Tengah', 'cartography' ),
		'SG' => __( 'Sulawesi Tenggara', 'cartography' ),
		'SR' => __( 'Sulawesi Barat', 'cartography' ),
		'SN' => __( 'Sulawesi Selatan', 'cartography' ),
		'GO' => __( 'Gorontalo', 'cartography' ),
		'MA' => __( 'Maluku', 'cartography' ),
		'MU' => __( 'Maluku Utara', 'cartography' ),
		'PA' => __( 'Papua', 'cartography' ),
		'PB' => __( 'Papua Barat', 'cartography' ),
	);

	return apply_filters( 'cartography_indonesian_provinces', $provinces );
}


/**
 * Get Indian states
 *
 * @since       1.0.0
 * @return      array $states A list of Indian states
 */
function cartography_get_indian_states_list() {
	$states = array(
		''   => '',
		'AP' => __( 'Andhra Pradesh', 'cartography' ),
		'AR' => __( 'Arunachal Pradesh', 'cartography' ),
		'AS' => __( 'Assam', 'cartography' ),
		'BR' => __( 'Bihar', 'cartography' ),
		'CT' => __( 'Chhattisgarh', 'cartography' ),
		'GA' => __( 'Goa', 'cartography' ),
		'GJ' => __( 'Gujarat', 'cartography' ),
		'HR' => __( 'Haryana', 'cartography' ),
		'HP' => __( 'Himachal Pradesh', 'cartography' ),
		'JK' => __( 'Jammu and Kashmir', 'cartography' ),
		'JH' => __( 'Jharkhand', 'cartography' ),
		'KA' => __( 'Karnataka', 'cartography' ),
		'KL' => __( 'Kerala', 'cartography' ),
		'MP' => __( 'Madhya Pradesh', 'cartography' ),
		'MH' => __( 'Maharashtra', 'cartography' ),
		'MN' => __( 'Manipur', 'cartography' ),
		'ML' => __( 'Meghalaya', 'cartography' ),
		'MZ' => __( 'Mizoram', 'cartography' ),
		'NL' => __( 'Nagaland', 'cartography' ),
		'OR' => __( 'Orissa', 'cartography' ),
		'PB' => __( 'Punjab', 'cartography' ),
		'RJ' => __( 'Rajasthan', 'cartography' ),
		'SK' => __( 'Sikkim', 'cartography' ),
		'TN' => __( 'Tamil Nadu', 'cartography' ),
		'TG' => __( 'Telangana', 'cartography' ),
		'TR' => __( 'Tripura', 'cartography' ),
		'UT' => __( 'Uttarakhand', 'cartography' ),
		'UP' => __( 'Uttar Pradesh', 'cartography' ),
		'WB' => __( 'West Bengal', 'cartography' ),
		'AN' => __( 'Andaman and Nicobar Islands', 'cartography' ),
		'CH' => __( 'Chandigarh', 'cartography' ),
		'DN' => __( 'Dadar and Nagar Haveli', 'cartography' ),
		'DD' => __( 'Daman and Diu', 'cartography' ),
		'DL' => __( 'Delhi', 'cartography' ),
		'LD' => __( 'Lakshadweep', 'cartography' ),
		'PY' => __( 'Pondicherry (Puducherry)', 'cartography' ),
	);

	return apply_filters( 'cartography_indian_states', $states );
}


/**
 * Get Iranian provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Iranian provinces.
 */
function cartography_get_iranian_provinces_list() {
	$provinces = array(
		''    => '',
		'KHZ' => __( 'Khuzestan', 'cartography' ),
		'THR' => __( 'Tehran', 'cartography' ),
		'ILM' => __( 'Ilaam', 'cartography' ),
		'BHR' => __( 'Bushehr', 'cartography' ),
		'ADL' => __( 'Ardabil', 'cartography' ),
		'ESF' => __( 'Isfahan', 'cartography' ),
		'YZD' => __( 'Yazd', 'cartography' ),
		'KRH' => __( 'Kermanshah', 'cartography' ),
		'KRN' => __( 'Kerman', 'cartography' ),
		'HDN' => __( 'Hamadan', 'cartography' ),
		'GZN' => __( 'Ghazvin', 'cartography' ),
		'ZJN' => __( 'Zanjan', 'cartography' ),
		'LRS' => __( 'Luristan', 'cartography' ),
		'ABZ' => __( 'Alborz', 'cartography' ),
		'EAZ' => __( 'East Azerbaijan', 'cartography' ),
		'WAZ' => __( 'West Azerbaijan', 'cartography' ),
		'CHB' => __( 'Chaharmahal and Bakhtiari', 'cartography' ),
		'SKH' => __( 'South Khorasan', 'cartography' ),
		'RKH' => __( 'Razavi Khorasan', 'cartography' ),
		'NKH' => __( 'North Khorasan', 'cartography' ),
		'SMN' => __( 'Semnan', 'cartography' ),
		'FRS' => __( 'Fars', 'cartography' ),
		'QHM' => __( 'Qom', 'cartography' ),
		'KRD' => __( 'Kurdistan', 'cartography' ),
		'KBD' => __( 'Kohgiluyeh and BoyerAhmad', 'cartography' ),
		'GLS' => __( 'Golestan', 'cartography' ),
		'GIL' => __( 'Gilan', 'cartography' ),
		'MZN' => __( 'Mazandaran', 'cartography' ),
		'MKZ' => __( 'Markazi', 'cartography' ),
		'HRZ' => __( 'Hormozgan', 'cartography' ),
		'SBN' => __( 'Sistan and Baluchestan', 'cartography' ),
	);

	return apply_filters( 'cartography_iranian_provinces', $provinces );
}


/**
 * Get Italian provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Italian provinces.
 */
function cartography_get_italian_provinces_list() {
	$provinces = array(
		''   => '',
		'AG' => __( 'Agrigento', 'cartography' ),
		'AL' => __( 'Alessandria', 'cartography' ),
		'AN' => __( 'Ancona', 'cartography' ),
		'AO' => __( 'Aosta', 'cartography' ),
		'AR' => __( 'Arezzo', 'cartography' ),
		'AP' => __( 'Ascoli Piceno', 'cartography' ),
		'AT' => __( 'Asti', 'cartography' ),
		'AV' => __( 'Avellino', 'cartography' ),
		'BA' => __( 'Bari', 'cartography' ),
		'BT' => __( 'Barletta-Andria-Trani', 'cartography' ),
		'BL' => __( 'Belluno', 'cartography' ),
		'BN' => __( 'Benevento', 'cartography' ),
		'BG' => __( 'Bergamo', 'cartography' ),
		'BI' => __( 'Biella', 'cartography' ),
		'BO' => __( 'Bologna', 'cartography' ),
		'BZ' => __( 'Bolzano', 'cartography' ),
		'BS' => __( 'Brescia', 'cartography' ),
		'BR' => __( 'Brindisi', 'cartography' ),
		'CA' => __( 'Cagliari', 'cartography' ),
		'CL' => __( 'Caltanissetta', 'cartography' ),
		'CB' => __( 'Campobasso', 'cartography' ),
		'CI' => __( 'Caltanissetta', 'cartography' ),
		'CE' => __( 'Caserta', 'cartography' ),
		'CT' => __( 'Catania', 'cartography' ),
		'CZ' => __( 'Catanzaro', 'cartography' ),
		'CH' => __( 'Chieti', 'cartography' ),
		'CO' => __( 'Como', 'cartography' ),
		'CS' => __( 'Cosenza', 'cartography' ),
		'CR' => __( 'Cremona', 'cartography' ),
		'KR' => __( 'Crotone', 'cartography' ),
		'CN' => __( 'Cuneo', 'cartography' ),
		'EN' => __( 'Enna', 'cartography' ),
		'FM' => __( 'Fermo', 'cartography' ),
		'FE' => __( 'Ferrara', 'cartography' ),
		'FI' => __( 'Firenze', 'cartography' ),
		'FG' => __( 'Foggia', 'cartography' ),
		'FC' => __( 'Forli-Cesena', 'cartography' ),
		'FR' => __( 'Frosinone', 'cartography' ),
		'GE' => __( 'Genoa', 'cartography' ),
		'GO' => __( 'Gorizia', 'cartography' ),
		'GR' => __( 'Grosseto', 'cartography' ),
		'IM' => __( 'Imperia', 'cartography' ),
		'IS' => __( 'Isernia', 'cartography' ),
		'SP' => __( 'La Spezia', 'cartography' ),
		'AQ' => __( 'L&apos;Aquila', 'cartography' ),
		'LT' => __( 'Latina', 'cartography' ),
		'LE' => __( 'Lecce', 'cartography' ),
		'LC' => __( 'Lecco', 'cartography' ),
		'LI' => __( 'Livorno', 'cartography' ),
		'LO' => __( 'Lodi', 'cartography' ),
		'LU' => __( 'Lucca', 'cartography' ),
		'MC' => __( 'Macerata', 'cartography' ),
		'MN' => __( 'Mantova', 'cartography' ),
		'MS' => __( 'Massa-Carrara', 'cartography' ),
		'MT' => __( 'Matera', 'cartography' ),
		'ME' => __( 'Messina', 'cartography' ),
		'MI' => __( 'Milano', 'cartography' ),
		'MO' => __( 'Modena', 'cartography' ),
		'MB' => __( 'Monza e della Brianza', 'cartography' ),
		'NA' => __( 'Napoli', 'cartography' ),
		'NO' => __( 'Novara', 'cartography' ),
		'NU' => __( 'Nuoro', 'cartography' ),
		'OT' => __( 'Olbia-Tempio', 'cartography' ),
		'OR' => __( 'Oristano', 'cartography' ),
		'PD' => __( 'Padova', 'cartography' ),
		'PA' => __( 'Palermo', 'cartography' ),
		'PR' => __( 'Parma', 'cartography' ),
		'PV' => __( 'Pavia', 'cartography' ),
		'PG' => __( 'Perugia', 'cartography' ),
		'PU' => __( 'Pesaro e Urbino', 'cartography' ),
		'PE' => __( 'Pescara', 'cartography' ),
		'PC' => __( 'Piacenza', 'cartography' ),
		'PI' => __( 'Pisa', 'cartography' ),
		'PT' => __( 'Pistoia', 'cartography' ),
		'PN' => __( 'Pordenone', 'cartography' ),
		'PZ' => __( 'Potenza', 'cartography' ),
		'PO' => __( 'Prato', 'cartography' ),
		'RG' => __( 'Ragusa', 'cartography' ),
		'RA' => __( 'Ravenna', 'cartography' ),
		'RC' => __( 'Reggio Calabria', 'cartography' ),
		'RE' => __( 'Reggio Emilia', 'cartography' ),
		'RI' => __( 'Rieti', 'cartography' ),
		'RN' => __( 'Rimini', 'cartography' ),
		'RM' => __( 'Roma', 'cartography' ),
		'RO' => __( 'Rovigo', 'cartography' ),
		'SA' => __( 'Salerno', 'cartography' ),
		'VS' => __( 'Medio Campidano', 'cartography' ),
		'SS' => __( 'Sassari', 'cartography' ),
		'SV' => __( 'Savona', 'cartography' ),
		'SI' => __( 'Siena', 'cartography' ),
		'SR' => __( 'Siracusa', 'cartography' ),
		'SO' => __( 'Sondrio', 'cartography' ),
		'TA' => __( 'Taranto', 'cartography' ),
		'TE' => __( 'Teramo', 'cartography' ),
		'TR' => __( 'Terni', 'cartography' ),
		'TO' => __( 'Torino', 'cartography' ),
		'OG' => __( 'Ogliastra', 'cartography' ),
		'TP' => __( 'Trapani', 'cartography' ),
		'TN' => __( 'Trento', 'cartography' ),
		'TV' => __( 'Treviso', 'cartography' ),
		'TS' => __( 'Trieste', 'cartography' ),
		'UD' => __( 'Udine', 'cartography' ),
		'VA' => __( 'Varesa', 'cartography' ),
		'VE' => __( 'Venezia', 'cartography' ),
		'VB' => __( 'Verbano-Cusio-Ossola', 'cartography' ),
		'VC' => __( 'Vercelli', 'cartography' ),
		'VR' => __( 'Verona', 'cartography' ),
		'VV' => __( 'Vibo Valentia', 'cartography' ),
		'VI' => __( 'Vicenza', 'cartography' ),
		'VT' => __( 'Viterbo', 'cartography' ),
	);

	return apply_filters( 'cartography_italian_provinces', $provinces );
}


/**
 * Get Malaysian states
 *
 * @since       1.0.0
 * @return      array $states A list of Malaysian states.
 */
function cartography_get_malaysian_states_list() {
	$states = array(
		''    => '',
		'JHR' => __( 'Johor', 'cartography' ),
		'KDH' => __( 'Kedah', 'cartography' ),
		'KTN' => __( 'Kelantan', 'cartography' ),
		'MLK' => __( 'Melaka', 'cartography' ),
		'NSN' => __( 'Negeri Sembilan', 'cartography' ),
		'PHG' => __( 'Pahang', 'cartography' ),
		'PRK' => __( 'Perak', 'cartography' ),
		'PLS' => __( 'Perlis', 'cartography' ),
		'PNG' => __( 'Pulau Pinang', 'cartography' ),
		'SBH' => __( 'Sabah', 'cartography' ),
		'SWK' => __( 'Sarawak', 'cartography' ),
		'SGR' => __( 'Selangor', 'cartography' ),
		'TRG' => __( 'Terengganu', 'cartography' ),
		'KUL' => __( 'W.P. Kuala Lumpur', 'cartography' ),
		'LBN' => __( 'W.P. Labuan', 'cartography' ),
		'PJY' => __( 'W.P. Putrajaya', 'cartography' ),
	);

	return apply_filters( 'cartography_malaysian_states', $states );
}


/**
 * Get Mexican states
 *
 * @since       1.0.0
 * @return      array $states A list of Mexican states.
 */
function cartography_get_mexican_states_list() {
	$states = array(
		''    => '',
		'DIF' => __( 'Distrito Federal', 'cartography' ),
		'JAL' => __( 'Jalisco', 'cartography' ),
		'NLE' => __( 'Nuevo Le&oacute;n', 'cartography' ),
		'AGU' => __( 'Aguascalientes', 'cartography' ),
		'BCN' => __( 'Baja California Norte', 'cartography' ),
		'BCS' => __( 'Baja California Sur', 'cartography' ),
		'CAM' => __( 'Campeche', 'cartography' ),
		'CHP' => __( 'Chiapas', 'cartography' ),
		'CHH' => __( 'Chihuahua', 'cartography' ),
		'COA' => __( 'Coahuila', 'cartography' ),
		'COL' => __( 'Colima', 'cartography' ),
		'DUR' => __( 'Durango', 'cartography' ),
		'GUA' => __( 'Guanajuato', 'cartography' ),
		'GRO' => __( 'Guerrero', 'cartography' ),
		'HID' => __( 'Hidalgo', 'cartography' ),
		'MEX' => __( 'Edo. de M&eacute;xico', 'cartography' ),
		'MIC' => __( 'Michoac&aacute;n', 'cartography' ),
		'MOR' => __( 'Morelos', 'cartography' ),
		'NAY' => __( 'Nayarit', 'cartography' ),
		'OAX' => __( 'Oaxaca', 'cartography' ),
		'PUE' => __( 'Puebla', 'cartography' ),
		'QUE' => __( 'Quer&eacute;taro', 'cartography' ),
		'ROO' => __( 'Quintana Roo', 'cartography' ),
		'SLP' => __( 'San Luis Potos&iacute;', 'cartography' ),
		'SIN' => __( 'Sinaloa', 'cartography' ),
		'SON' => __( 'Sonora', 'cartography' ),
		'TAB' => __( 'Tabasco', 'cartography' ),
		'TAM' => __( 'Tamaulipas', 'cartography' ),
		'TLA' => __( 'Tlaxcala', 'cartography' ),
		'VER' => __( 'Veracruz', 'cartography' ),
		'YUC' => __( 'Yucat&aacute;n', 'cartography' ),
		'ZAC' => __( 'Zacatecas', 'cartography' ),
	);

	return apply_filters( 'cartography_mexican_states', $states );
}


/**
 * Get Nepalese districts
 *
 * @since       1.0.0
 * @return      array $districts A list of Nepalese districts.
 */
function cartography_get_nepalese_districts_list() {
	$districts = array(
		''    => '',
		'ILL' => __( 'Illam', 'cartography' ),
		'JHA' => __( 'Jhapa', 'cartography' ),
		'PAN' => __( 'Panchthar', 'cartography' ),
		'TAP' => __( 'Taplejung', 'cartography' ),
		'BHO' => __( 'Bhojpur', 'cartography' ),
		'DKA' => __( 'Dhankuta', 'cartography' ),
		'MOR' => __( 'Morang', 'cartography' ),
		'SUN' => __( 'Sunsari', 'cartography' ),
		'SAN' => __( 'Sankhuwa', 'cartography' ),
		'TER' => __( 'Terhathum', 'cartography' ),
		'KHO' => __( 'Khotang', 'cartography' ),
		'OKH' => __( 'Okhaldhunga', 'cartography' ),
		'SAP' => __( 'Saptari', 'cartography' ),
		'SIR' => __( 'Siraha', 'cartography' ),
		'SOL' => __( 'Solukhumbu', 'cartography' ),
		'UDA' => __( 'Udayapur', 'cartography' ),
		'DHA' => __( 'Dhanusa', 'cartography' ),
		'DLK' => __( 'Dolakha', 'cartography' ),
		'MOH' => __( 'Mohottari', 'cartography' ),
		'RAM' => __( 'Ramechha', 'cartography' ),
		'SAR' => __( 'Sarlahi', 'cartography' ),
		'SIN' => __( 'Sindhuli', 'cartography' ),
		'BHA' => __( 'Bhaktapur', 'cartography' ),
		'DHD' => __( 'Dhading', 'cartography' ),
		'KTM' => __( 'Kathmandu', 'cartography' ),
		'KAV' => __( 'Kavrepalanchowk', 'cartography' ),
		'LAL' => __( 'Lalitpur', 'cartography' ),
		'NUW' => __( 'Nuwakot', 'cartography' ),
		'RAS' => __( 'Rasuwa', 'cartography' ),
		'SPC' => __( 'Sindhupalchowk', 'cartography' ),
		'BAR' => __( 'Bara', 'cartography' ),
		'CHI' => __( 'Chitwan', 'cartography' ),
		'MAK' => __( 'Makwanpur', 'cartography' ),
		'PAR' => __( 'Parsa', 'cartography' ),
		'RAU' => __( 'Rautahat', 'cartography' ),
		'GOR' => __( 'Gorkha', 'cartography' ),
		'KAS' => __( 'Kaski', 'cartography' ),
		'LAM' => __( 'Lamjung', 'cartography' ),
		'MAN' => __( 'Manang', 'cartography' ),
		'SYN' => __( 'Syangja', 'cartography' ),
		'TAN' => __( 'Tanahun', 'cartography' ),
		'BAG' => __( 'Baglung', 'cartography' ),
		'PBT' => __( 'Parbat', 'cartography' ),
		'MUS' => __( 'Mustang', 'cartography' ),
		'MYG' => __( 'Myagdi', 'cartography' ),
		'AGR' => __( 'Agrghakanchi', 'cartography' ),
		'GUL' => __( 'Gulmi', 'cartography' ),
		'KAP' => __( 'Kapilbastu', 'cartography' ),
		'NAW' => __( 'Nawalparasi', 'cartography' ),
		'PAL' => __( 'Palpa', 'cartography' ),
		'RUP' => __( 'Rupandehi', 'cartography' ),
		'DAN' => __( 'Dang', 'cartography' ),
		'PYU' => __( 'Pyuthan', 'cartography' ),
		'ROL' => __( 'Rolpa', 'cartography' ),
		'RUK' => __( 'Rukum', 'cartography' ),
		'SAL' => __( 'Salyan', 'cartography' ),
		'BAN' => __( 'Banke', 'cartography' ),
		'BDA' => __( 'Bardiya', 'cartography' ),
		'DAI' => __( 'Dailekh', 'cartography' ),
		'JAJ' => __( 'Jajarkot', 'cartography' ),
		'SUR' => __( 'Surkhet', 'cartography' ),
		'DOL' => __( 'Dolpa', 'cartography' ),
		'HUM' => __( 'Humla', 'cartography' ),
		'JUM' => __( 'Jumla', 'cartography' ),
		'KAL' => __( 'Kalikot', 'cartography' ),
		'MUG' => __( 'Mugu', 'cartography' ),
		'ACH' => __( 'Achham', 'cartography' ),
		'BJH' => __( 'Bajhang', 'cartography' ),
		'BJU' => __( 'Bajura', 'cartography' ),
		'DOT' => __( 'Doti', 'cartography' ),
		'KAI' => __( 'Kailali', 'cartography' ),
		'BAI' => __( 'Baitadi', 'cartography' ),
		'DAD' => __( 'Dadeldhura', 'cartography' ),
		'DAR' => __( 'Darchula', 'cartography' ),
		'KAN' => __( 'Kanchanpur', 'cartography' ),
	);

	return apply_filters( 'cartography_nepalese_states', $states );
}


/**
 * Get South African provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of provinces.
 */
function cartography_get_south_african_states_list() {
	$provinces = array(
		''    => '',
		'EC'  => __( 'Eastern Cape', 'cartography' ),
		'FS'  => __( 'Free State', 'cartography' ),
		'GP'  => __( 'Gauteng', 'cartography' ),
		'KZN' => __( 'KwaZulu-Natal', 'cartography' ),
		'LP'  => __( 'Limpopo', 'cartography' ),
		'MP'  => __( 'Mpumalanga', 'cartography' ),
		'NC'  => __( 'Northern Cape', 'cartography' ),
		'NW'  => __( 'North West', 'cartography' ),
		'WC'  => __( 'Western Cape', 'cartography' ),
	);

	return apply_filters( 'cartography_south_african_provinces', $provinces );
}


/**
 * Get Thai provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Thai provinces.
 */
function cartography_get_thai_provinces_list() {
	$provinces = array(
		''      => '',
		'TH-37' => __( 'Amnat Charoen (&#3629;&#3635;&#3609;&#3634;&#3592;&#3648;&#3592;&#3619;&#3636;&#3597;)', 'cartography' ),
		'TH-15' => __( 'Ang Thong (&#3629;&#3656;&#3634;&#3591;&#3607;&#3629;&#3591;)', 'cartography' ),
		'TH-14' => __( 'Ayutthaya (&#3614;&#3619;&#3632;&#3609;&#3588;&#3619;&#3624;&#3619;&#3637;&#3629;&#3618;&#3640;&#3608;&#3618;&#3634;)', 'cartography' ),
		'TH-10' => __( 'Bangkok (&#3585;&#3619;&#3640;&#3591;&#3648;&#3607;&#3614;&#3617;&#3627;&#3634;&#3609;&#3588;&#3619;)', 'cartography' ),
		'TH-38' => __( 'Bueng Kan (&#3610;&#3638;&#3591;&#3585;&#3634;&#3628;)', 'cartography' ),
		'TH-31' => __( 'Buri Ram (&#3610;&#3640;&#3619;&#3637;&#3619;&#3633;&#3617;&#3618;&#3660;)', 'cartography' ),
		'TH-24' => __( 'Chachoengsao (&#3593;&#3632;&#3648;&#3594;&#3636;&#3591;&#3648;&#3607;&#3619;&#3634;)', 'cartography' ),
		'TH-18' => __( 'Chai Nat (&#3594;&#3633;&#3618;&#3609;&#3634;&#3607;)', 'cartography' ),
		'TH-36' => __( 'Chaiyaphum (&#3594;&#3633;&#3618;&#3616;&#3641;&#3617;&#3636;)', 'cartography' ),
		'TH-22' => __( 'Chanthaburi (&#3592;&#3633;&#3609;&#3607;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-50' => __( 'Chiang Mai (&#3648;&#3594;&#3637;&#3618;&#3591;&#3651;&#3627;&#3617;&#3656;)', 'cartography' ),
		'TH-57' => __( 'Chiang Rai (&#3648;&#3594;&#3637;&#3618;&#3591;&#3619;&#3634;&#3618;)', 'cartography' ),
		'TH-20' => __( 'Chonburi (&#3594;&#3621;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-86' => __( 'Chumphon (&#3594;&#3640;&#3617;&#3614;&#3619;)', 'cartography' ),
		'TH-46' => __( 'Kalasin (&#3585;&#3634;&#3628;&#3626;&#3636;&#3609;&#3608;&#3640;&#3660;)', 'cartography' ),
		'TH-62' => __( 'Kamphaeng Phet (&#3585;&#3635;&#3649;&#3614;&#3591;&#3648;&#3614;&#3594;&#3619;)', 'cartography' ),
		'TH-71' => __( 'Kanchanaburi (&#3585;&#3634;&#3597;&#3592;&#3609;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-40' => __( 'Khon Kaen (&#3586;&#3629;&#3609;&#3649;&#3585;&#3656;&#3609;)', 'cartography' ),
		'TH-81' => __( 'Krabi (&#3585;&#3619;&#3632;&#3610;&#3637;&#3656;)', 'cartography' ),
		'TH-52' => __( 'Lampang (&#3621;&#3635;&#3611;&#3634;&#3591;)', 'cartography' ),
		'TH-51' => __( 'Lamphun (&#3621;&#3635;&#3614;&#3641;&#3609;)', 'cartography' ),
		'TH-42' => __( 'Loei (&#3648;&#3621;&#3618;)', 'cartography' ),
		'TH-16' => __( 'Lopburi (&#3621;&#3614;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-58' => __( 'Mae Hong Son (&#3649;&#3617;&#3656;&#3630;&#3656;&#3629;&#3591;&#3626;&#3629;&#3609;)', 'cartography' ),
		'TH-44' => __( 'Maha Sarakham (&#3617;&#3627;&#3634;&#3626;&#3634;&#3619;&#3588;&#3634;&#3617;)', 'cartography' ),
		'TH-49' => __( 'Mukdahan (&#3617;&#3640;&#3585;&#3604;&#3634;&#3627;&#3634;&#3619;)', 'cartography' ),
		'TH-26' => __( 'Nakhon Nayok (&#3609;&#3588;&#3619;&#3609;&#3634;&#3618;&#3585;)', 'cartography' ),
		'TH-73' => __( 'Nakhon Pathom (&#3609;&#3588;&#3619;&#3611;&#3600;&#3617;)', 'cartography' ),
		'TH-48' => __( 'Nakhon Phanom (&#3609;&#3588;&#3619;&#3614;&#3609;&#3617;)', 'cartography' ),
		'TH-30' => __( 'Nakhon Ratchasima (&#3609;&#3588;&#3619;&#3619;&#3634;&#3594;&#3626;&#3637;&#3617;&#3634;)', 'cartography' ),
		'TH-60' => __( 'Nakhon Sawan (&#3609;&#3588;&#3619;&#3626;&#3623;&#3619;&#3619;&#3588;&#3660;)', 'cartography' ),
		'TH-80' => __( 'Nakhon Si Thammarat (&#3609;&#3588;&#3619;&#3624;&#3619;&#3637;&#3608;&#3619;&#3619;&#3617;&#3619;&#3634;&#3594;)', 'cartography' ),
		'TH-55' => __( 'Nan (&#3609;&#3656;&#3634;&#3609;)', 'cartography' ),
		'TH-96' => __( 'Narathiwat (&#3609;&#3619;&#3634;&#3608;&#3636;&#3623;&#3634;&#3626;)', 'cartography' ),
		'TH-39' => __( 'Nong Bua Lam Phu (&#3627;&#3609;&#3629;&#3591;&#3610;&#3633;&#3623;&#3621;&#3635;&#3616;&#3641;)', 'cartography' ),
		'TH-43' => __( 'Nong Khai (&#3627;&#3609;&#3629;&#3591;&#3588;&#3634;&#3618;)', 'cartography' ),
		'TH-12' => __( 'Nonthaburi (&#3609;&#3609;&#3607;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-13' => __( 'Pathum Thani (&#3611;&#3607;&#3640;&#3617;&#3608;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-94' => __( 'Pattani (&#3611;&#3633;&#3605;&#3605;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-82' => __( 'Phang Nga (&#3614;&#3633;&#3591;&#3591;&#3634;)', 'cartography' ),
		'TH-93' => __( 'Phatthalung (&#3614;&#3633;&#3607;&#3621;&#3640;&#3591;)', 'cartography' ),
		'TH-56' => __( 'Phayao (&#3614;&#3632;&#3648;&#3618;&#3634;)', 'cartography' ),
		'TH-67' => __( 'Phetchabun (&#3648;&#3614;&#3594;&#3619;&#3610;&#3641;&#3619;&#3603;&#3660;)', 'cartography' ),
		'TH-76' => __( 'Phetchaburi (&#3648;&#3614;&#3594;&#3619;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-66' => __( 'Phichit (&#3614;&#3636;&#3592;&#3636;&#3605;&#3619;)', 'cartography' ),
		'TH-65' => __( 'Phitsanulok (&#3614;&#3636;&#3625;&#3603;&#3640;&#3650;&#3621;&#3585;)', 'cartography' ),
		'TH-54' => __( 'Phrae (&#3649;&#3614;&#3619;&#3656;)', 'cartography' ),
		'TH-83' => __( 'Phuket (&#3616;&#3641;&#3648;&#3585;&#3655;&#3605;)', 'cartography' ),
		'TH-25' => __( 'Prachin Buri (&#3611;&#3619;&#3634;&#3592;&#3637;&#3609;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-77' => __( 'Prachuap Khiri Khan (&#3611;&#3619;&#3632;&#3592;&#3623;&#3610;&#3588;&#3637;&#3619;&#3637;&#3586;&#3633;&#3609;&#3608;&#3660;)', 'cartography' ),
		'TH-85' => __( 'Ranong (&#3619;&#3632;&#3609;&#3629;&#3591;)', 'cartography' ),
		'TH-70' => __( 'Ratchaburi (&#3619;&#3634;&#3594;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-21' => __( 'Rayong (&#3619;&#3632;&#3618;&#3629;&#3591;)', 'cartography' ),
		'TH-45' => __( 'Roi Et (&#3619;&#3657;&#3629;&#3618;&#3648;&#3629;&#3655;&#3604;)', 'cartography' ),
		'TH-27' => __( 'Sa Kaeo (&#3626;&#3619;&#3632;&#3649;&#3585;&#3657;&#3623;)', 'cartography' ),
		'TH-47' => __( 'Sakon Nakhon (&#3626;&#3585;&#3621;&#3609;&#3588;&#3619;)', 'cartography' ),
		'TH-11' => __( 'Samut Prakan (&#3626;&#3617;&#3640;&#3607;&#3619;&#3611;&#3619;&#3634;&#3585;&#3634;&#3619;)', 'cartography' ),
		'TH-74' => __( 'Samut Sakhon (&#3626;&#3617;&#3640;&#3607;&#3619;&#3626;&#3634;&#3588;&#3619;)', 'cartography' ),
		'TH-75' => __( 'Samut Songkhram (&#3626;&#3617;&#3640;&#3607;&#3619;&#3626;&#3591;&#3588;&#3619;&#3634;&#3617;)', 'cartography' ),
		'TH-19' => __( 'Saraburi (&#3626;&#3619;&#3632;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-91' => __( 'Satun (&#3626;&#3605;&#3641;&#3621;)', 'cartography' ),
		'TH-17' => __( 'Sing Buri (&#3626;&#3636;&#3591;&#3627;&#3660;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-33' => __( 'Sisaket (&#3624;&#3619;&#3637;&#3626;&#3632;&#3648;&#3585;&#3625;)', 'cartography' ),
		'TH-90' => __( 'Songkhla (&#3626;&#3591;&#3586;&#3621;&#3634;)', 'cartography' ),
		'TH-64' => __( 'Sukhothai (&#3626;&#3640;&#3650;&#3586;&#3607;&#3633;&#3618;)', 'cartography' ),
		'TH-72' => __( 'Suphan Buri (&#3626;&#3640;&#3614;&#3619;&#3619;&#3603;&#3610;&#3640;&#3619;&#3637;)', 'cartography' ),
		'TH-84' => __( 'Surat Thani (&#3626;&#3640;&#3619;&#3634;&#3625;&#3598;&#3619;&#3660;&#3608;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-32' => __( 'Surin (&#3626;&#3640;&#3619;&#3636;&#3609;&#3607;&#3619;&#3660;)', 'cartography' ),
		'TH-63' => __( 'Tak (&#3605;&#3634;&#3585;)', 'cartography' ),
		'TH-92' => __( 'Trang (&#3605;&#3619;&#3633;&#3591;)', 'cartography' ),
		'TH-23' => __( 'Trat (&#3605;&#3619;&#3634;&#3604;)', 'cartography' ),
		'TH-34' => __( 'Ubon Ratchathani (&#3629;&#3640;&#3610;&#3621;&#3619;&#3634;&#3594;&#3608;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-41' => __( 'Udon Thani (&#3629;&#3640;&#3604;&#3619;&#3608;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-61' => __( 'Uthai Thani (&#3629;&#3640;&#3607;&#3633;&#3618;&#3608;&#3634;&#3609;&#3637;)', 'cartography' ),
		'TH-53' => __( 'Uttaradit (&#3629;&#3640;&#3605;&#3619;&#3604;&#3636;&#3605;&#3606;&#3660;)', 'cartography' ),
		'TH-95' => __( 'Yala (&#3618;&#3632;&#3621;&#3634;)', 'cartography' ),
		'TH-35' => __( 'Yasothon (&#3618;&#3650;&#3626;&#3608;&#3619;)', 'cartography' ),
	);

	return apply_filters( 'cartography_thai_provinces', $provinces );
}


/**
 * Get Turkish provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Turkish provinces.
 */
function cartography_get_turkish_provinces_list() {
	$provinces = array(
		''     => '',
		'TR01' => __( 'Adana', 'cartography' ),
		'TR02' => __( 'Ad&#305;yaman', 'cartography' ),
		'TR03' => __( 'Afyon', 'cartography' ),
		'TR04' => __( 'A&#287;r&#305;', 'cartography' ),
		'TR05' => __( 'Amasya', 'cartography' ),
		'TR06' => __( 'Ankara', 'cartography' ),
		'TR07' => __( 'Antalya', 'cartography' ),
		'TR08' => __( 'Artvin', 'cartography' ),
		'TR09' => __( 'Ayd&#305;n', 'cartography' ),
		'TR10' => __( 'Bal&#305;kesir', 'cartography' ),
		'TR11' => __( 'Bilecik', 'cartography' ),
		'TR12' => __( 'Bing&#246;l', 'cartography' ),
		'TR13' => __( 'Bitlis', 'cartography' ),
		'TR14' => __( 'Bolu', 'cartography' ),
		'TR15' => __( 'Burdur', 'cartography' ),
		'TR16' => __( 'Bursa', 'cartography' ),
		'TR17' => __( '&#199;anakkale', 'cartography' ),
		'TR18' => __( '&#199;ank&#305;kesir', 'cartography' ),
		'TR19' => __( '&#199;orum', 'cartography' ),
		'TR20' => __( 'Denizli', 'cartography' ),
		'TR21' => __( 'Diyarbak&#305;r', 'cartography' ),
		'TR22' => __( 'Edirne', 'cartography' ),
		'TR23' => __( 'Elaz&#305;&#287;', 'cartography' ),
		'TR24' => __( 'Erzincan', 'cartography' ),
		'TR25' => __( 'Erzurum', 'cartography' ),
		'TR26' => __( 'Eski&#351;ehir', 'cartography' ),
		'TR27' => __( 'Gaziantep', 'cartography' ),
		'TR28' => __( 'Giresun', 'cartography' ),
		'TR29' => __( 'G&#252;m&#252;&#351;hane', 'cartography' ),
		'TR30' => __( 'Hakkari', 'cartography' ),
		'TR31' => __( 'Hatay', 'cartography' ),
		'TR32' => __( 'Isparta', 'cartography' ),
		'TR33' => __( '&#304;&#231;el', 'cartography' ),
		'TR34' => __( '&#304;stanbul', 'cartography' ),
		'TR35' => __( '&#304;zmir', 'cartography' ),
		'TR36' => __( 'Kars', 'cartography' ),
		'TR37' => __( 'Kastamonu', 'cartography' ),
		'TR38' => __( 'Kayseri', 'cartography' ),
		'TR39' => __( 'K&#305;rklareli', 'cartography' ),
		'TR40' => __( 'K&#305;r&#351;ehir', 'cartography' ),
		'TR41' => __( 'Kocaeli', 'cartography' ),
		'TR42' => __( 'Konya', 'cartography' ),
		'TR43' => __( 'K&#252;tahya', 'cartography' ),
		'TR44' => __( 'Malatya', 'cartography' ),
		'TR45' => __( 'Manisa', 'cartography' ),
		'TR46' => __( 'Kahramanmara&#351;', 'cartography' ),
		'TR47' => __( 'Mardin', 'cartography' ),
		'TR48' => __( 'Mu&#287;la', 'cartography' ),
		'TR49' => __( 'Mu&#351;', 'cartography' ),
		'TR50' => __( 'Nev&#351;ehir', 'cartography' ),
		'TR51' => __( 'Ni&#287;de', 'cartography' ),
		'TR52' => __( 'Ordu', 'cartography' ),
		'TR53' => __( 'Rize', 'cartography' ),
		'TR54' => __( 'Sakarya', 'cartography' ),
		'TR55' => __( 'Samsun', 'cartography' ),
		'TR56' => __( 'Siirt', 'cartography' ),
		'TR57' => __( 'Sinop', 'cartography' ),
		'TR58' => __( 'Sivas', 'cartography' ),
		'TR59' => __( 'Tekirda&#287;', 'cartography' ),
		'TR60' => __( 'Tokat', 'cartography' ),
		'TR61' => __( 'Trabzon', 'cartography' ),
		'TR62' => __( 'Tunceli', 'cartography' ),
		'TR63' => __( '&#350;anl&#305;urfa', 'cartography' ),
		'TR64' => __( 'U&#351;ak', 'cartography' ),
		'TR65' => __( 'Van', 'cartography' ),
		'TR66' => __( 'Yozgat', 'cartography' ),
		'TR67' => __( 'Zonguldak', 'cartography' ),
		'TR68' => __( 'Aksaray', 'cartography' ),
		'TR69' => __( 'Bayburt', 'cartography' ),
		'TR70' => __( 'Karaman', 'cartography' ),
		'TR71' => __( 'K&#305;r&#305;kkale', 'cartography' ),
		'TR72' => __( 'Batman', 'cartography' ),
		'TR73' => __( '&#350;&#305;rnak', 'cartography' ),
		'TR74' => __( 'Bart&#305;n', 'cartography' ),
		'TR75' => __( 'Ardahan', 'cartography' ),
		'TR76' => __( 'I&#287;d&#305;r', 'cartography' ),
		'TR77' => __( 'Yalova', 'cartography' ),
		'TR78' => __( 'Karab&#252;k', 'cartography' ),
		'TR79' => __( 'Kilis', 'cartography' ),
		'TR80' => __( 'Osmaniye', 'cartography' ),
		'TR81' => __( 'D&#252;zce', 'cartography' ),
	);

	return apply_filters( 'cartography_turkish_provinces', $provinces );
}


/**
 * Get Spanish provinces
 *
 * @since       1.0.0
 * @return      array $provinces A list of Spanish provinces.
 */
function cartography_get_spanish_provinces_list() {
	$provinces = array(
		''   => '',
		'C'  => __( 'A Coru&ntilde;a', 'cartography' ),
		'VI' => __( 'Araba', 'cartography' ),
		'AB' => __( 'Albacete', 'cartography' ),
		'A'  => __( 'Alicante', 'cartography' ),
		'AL' => __( 'Almer&iacute;a', 'cartography' ),
		'O'  => __( 'Asturias', 'cartography' ),
		'AV' => __( '&Aacute;vila', 'cartography' ),
		'BA' => __( 'Badajoz', 'cartography' ),
		'PM' => __( 'Baleares', 'cartography' ),
		'B'  => __( 'Barcelona', 'cartography' ),
		'BU' => __( 'Burgos', 'cartography' ),
		'CC' => __( 'C&aacute;ceres', 'cartography' ),
		'CA' => __( 'C&aacute;diz', 'cartography' ),
		'S'  => __( 'Cantabria', 'cartography' ),
		'CS' => __( 'Castell&oacute;n', 'cartography' ),
		'CE' => __( 'Ceuta', 'cartography' ),
		'CR' => __( 'Ciudad Real', 'cartography' ),
		'CO' => __( 'C&oacute;rdoba', 'cartography' ),
		'CU' => __( 'Cuenca', 'cartography' ),
		'GI' => __( 'Girona', 'cartography' ),
		'GR' => __( 'Granada', 'cartography' ),
		'GU' => __( 'Guadalajara', 'cartography' ),
		'SS' => __( 'Gipuzkoa', 'cartography' ),
		'H'  => __( 'Huelva', 'cartography' ),
		'HU' => __( 'Huesca', 'cartography' ),
		'J'  => __( 'Ja&eacute;n', 'cartography' ),
		'LO' => __( 'La Rioja', 'cartography' ),
		'GC' => __( 'Las Palmas', 'cartography' ),
		'LE' => __( 'Le&oacute;n', 'cartography' ),
		'L'  => __( 'Lleida', 'cartography' ),
		'LU' => __( 'Lugo', 'cartography' ),
		'M'  => __( 'Madrid', 'cartography' ),
		'MA' => __( 'M&aacute;laga', 'cartography' ),
		'ML' => __( 'Melilla', 'cartography' ),
		'MU' => __( 'Murcia', 'cartography' ),
		'NA' => __( 'Navarra', 'cartography' ),
		'OR' => __( 'Ourense', 'cartography' ),
		'P'  => __( 'Palencia', 'cartography' ),
		'PO' => __( 'Pontevedra', 'cartography' ),
		'SA' => __( 'Salamanca', 'cartography' ),
		'TF' => __( 'Santa Cruz de Tenerife', 'cartography' ),
		'SG' => __( 'Segovia', 'cartography' ),
		'SE' => __( 'Sevilla', 'cartography' ),
		'SO' => __( 'Soria', 'cartography' ),
		'T'  => __( 'Tarragona', 'cartography' ),
		'TE' => __( 'Teruel', 'cartography' ),
		'TO' => __( 'Toledo', 'cartography' ),
		'V'  => __( 'Valencia', 'cartography' ),
		'VA' => __( 'Valladolid', 'cartography' ),
		'BI' => __( 'Bizkaia', 'cartography' ),
		'ZA' => __( 'Zamora', 'cartography' ),
		'Z'  => __( 'Zaragoza', 'cartography' ),
	);

	return apply_filters( 'cartography_spanish_provinces', $provinces );
}


/**
 * Given a country code, return the country name
 *
 * @since       1.0.0
 * @param       string $country_code The ISO Code for the country.
 * @return      string The parsed country name.
 */
function cartography_get_country_name( $country_code = '' ) {
	$country_list = cartography_get_country_list();
	$country_name = isset( $country_list[ $country_code ] ) ? $country_list[ $country_code ] : $country_code;

	return apply_filters( 'cartography_get_country_name', $country_name, $country_code );
}


/**
 * Given a country and state code, return the state name
 *
 * @since       1.0.0
 * @param       string $country_code The ISO Code for the country.
 * @param       string $state_code The ISO Code for the state.
 * @return      string The parsed state/province name.
 */
function cartography_get_state_name( $country_code = '', $state_code = '' ) {
	$states_list = cartography_get_shop_states( $country_code );
	$state_name  = isset( $states_list[ $state_code ] ) ? $states_list[ $state_code ] : $state_code;

	return apply_filters( 'cartography_get_state_name', $state_name, $state_code );
}
