<?php
/**
 * Location meta boxes
 *
 * @package     Cartography\Location\MetaBoxes
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Add location meta boxes.
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_location_add_meta_boxes() {
	add_meta_box(
		'cartography-location-info',
		__( 'Location Information', 'cartography' ),
		'cartography_location_render_info_meta_box',
		'location',
		'normal',
		'high'
	);

	add_meta_box(
		'cartography-location-contact',
		__( 'Location Contact', 'cartography' ),
		'cartography_location_render_contact_meta_box',
		'location',
		'side',
		'default'
	);
}
add_action( 'add_meta_boxes', 'cartography_location_add_meta_boxes' );


/**
 * Location info meta box
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_location_render_info_meta_box() {
	global $post;

	do_action( 'cartography_location_meta_box_info_fields', $post->ID );
}


/**
 * Location address
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_location_render_address_fields( $post_id = 0 ) {
	$countries = cartography_get_country_list();
	$defaults  = array(
		'address1' => '',
		'address2' => '',
		'city'     => '',
		'state'    => '',
		'zip'      => '',
		'country'  => 'US',
	);
	$location  = get_post_meta( $post_id, 'location_address', true );
	$location  = is_array( $location ) ? $location : $defaults;
	$states    = cartography_get_states( $location['country'] );
	?>
	<span class="cartography-location-row">
		<span class="cartography-location-cols">
			<span class="cartography-location-col">
				<p id="cartography-location-address-wrap">
					<label class="cartography-label" for="cartography-location-address"><?php esc_html_e( 'Address:', 'cartography' ); ?></label>

					<span id="cartography-location-address-row1" class="cartography-location-row">
						<input type="text" name="location_address[address1]" id="cartography-location-address1" value="<?php echo esc_attr( $location['address1'] ); ?>" />
					</span>

					<span id="cartography-location-address-row2" class="cartography-location-row">
						<input type="text" name="location_address[address2]" id="cartography-location-address2" value="<?php echo esc_attr( $location['address2'] ); ?>" />
					</span>

					<span id="cartography-location-address-row3" class="cartography-location-row">
						<span class="cartography-location-cols">
							<span class="cartography-location-col">
								<input type="text" name="location_address[city]" id="cartography-location-city" value="<?php echo esc_attr( $location['city'] ); ?>" />
								<label for="cartography-location-city" class="description"><?php esc_html_e( 'City', 'cartography' ); ?></label>
							</span>
							<span class="cartography-location-col">
								<?php
								if ( ! empty( $states ) ) {
									echo '<select name="location_address[state]" id="cartography-location-state">';

									foreach ( $states as $state_code => $state_name ) {
										$selected = ( $location['state'] === $state_code ) ? ' selected="selected"' : '';

										echo '<option value="' . esc_attr( $state_code ) . '"' . esc_attr( $selected ) . '>' . esc_html( $state_name ) . '</option>';
									}

									echo '</select>';
								} else {
									echo '<input type="text" name="location_address[state]" id="cartography-location-state" value="' . esc_attr( $location['state'] ) . '" />';
								}
								?>
								<label for="cartography-location-state" class="description"><?php esc_html_e( 'State/Province', 'cartography' ); ?></label>
							</span>
							<span class="cartography-location-col">
								<input type="text" name="location_address[zip]" id="cartography-location-zip" value="<?php echo esc_attr( $location['zip'] ); ?>" />
								<label for="cartography-location-zip" class="description"><?php esc_html_e( 'Zip/Postal Code', 'cartography' ); ?></label>
							</span>
						</span>
					</span>

					<span id="cartography-location-address-row4" class="cartography-location-row">
						<select name="location_address[country]" id="cartography-location-country" class="cartography-select2" data-nonce="<?php echo esc_attr( wp_create_nonce( 'cartography-location-country-nonce' ) ); ?>">
							<?php
							foreach ( $countries as $country_code => $country_name ) {
								$selected = ( $location['country'] === $country_code ) ? ' selected="selected"' : '';

								echo '<option value="' . esc_attr( $country_code ) . '"' . esc_attr( $selected ) . '>' . esc_html( $country_name ) . '</option>';
							}
							?>
						</select>
						<label for="cartography-location-country" class="description"><?php esc_html_e( 'Country', 'cartography' ); ?></label>
					</span>
				</p>
				<?php
				$default = array(
					'latitude'  => '',
					'longitude' => '',
				);
				$coords  = get_post_meta( $post_id, 'location_coords', true );
				$coords  = is_array( $coords ) ? $coords : $default;
				?>

				<a id="cartography-location-coords-toggle"><?php esc_html_e( 'Advanced Options', 'cartography' ); ?></a>
				<p id="cartography-location-coords-wrap">
					<label class="cartography-label" for="cartography-location-coords-latitude"><?php esc_html_e( 'Coordinates:', 'cartography' ); ?></label>

					<span class="cartography-location-row">
						<span class="cartography-location-cols">
							<span class="cartography-location-col">
								<input type="text" name="location_coords[latitude]" id="cartography-location-coords-latitude" value="<?php echo esc_attr( $coords['latitude'] ); ?>" />
								<label class="description" for="cartography-location-coords-latitude"><?php esc_html_e( 'Latitude', 'cartography' ); ?></label>
							</span>

							<span class="cartography-location-col">
								<input type="text" name="location_coords[longitude]" id="cartography-location-coords-longitude" value="<?php echo esc_attr( $coords['longitude'] ); ?>" />
								<label class="description" for="cartography-location-coords-longitude"><?php esc_html_e( 'Longitude', 'cartography' ); ?></label>
							</span>
						</span>
					</span>
				</p>

				<p id="cartography-location-coords-set">
					<input type="button" name="cartography_location_set" id="cartography-location-set" class="button" value="<?php esc_attr_e( 'Set Address On Map', 'cartography' ); ?>" />
				</p>
			</span>

			<span class="cartography-location-col">
				<?php
				$type = cartography()->settings->get_option( 'google_maps_view', 'default' );
				$zoom = cartography()->settings->get_option( 'google_maps_zoom', 8 );
				$pin  = cartography()->settings->get_option( 'google_maps_pin', '' );
				?>
				<p id="cartography-location-map-wrap">
					<div id="cartography-location-map"
						data-type="<?php echo esc_attr( $type ); ?>"
						data-zoom="<?php echo esc_attr( absint( $zoom ) ); ?>"
						data-pin="<?php echo esc_url( $pin ); ?>"></div>
				</p>
			</span>
		</span>
	</span>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_location_meta_box_nonce' );
}
add_action( 'cartography_location_meta_box_info_fields', 'cartography_location_render_address_fields', 10 );


/**
 * Location contact meta box
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_location_render_contact_meta_box() {
	global $post;

	do_action( 'cartography_location_meta_box_contact_fields', $post->ID );
}


/**
 * Location contact name
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_location_render_contact_name_field( $post_id = 0 ) {
	$contact = get_post_meta( $post_id, 'location_contact', true );
	?>
	<p>
		<label class="cartography-label" for="cartography-location-contact-name"><?php esc_html_e( 'Name:', 'cartography' ); ?></label>
		<input type="text" name="location_contact[name]" id="cartography-location-contact-name" class="widefat" value="<?php echo esc_attr( $contact['name'] ); ?>" />
	</p>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_location_meta_box_nonce' );
}
add_action( 'cartography_location_meta_box_contact_fields', 'cartography_location_render_contact_name_field', 10 );


/**
 * Location contact email
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_location_render_contact_email_field( $post_id = 0 ) {
	$contact = get_post_meta( $post_id, 'location_contact', true );
	?>
	<p>
		<label class="cartography-label" for="cartography-location-contact-email"><?php esc_html_e( 'Email:', 'cartography' ); ?></label>
		<input type="email" name="location_contact[email]" id="cartography-location-contact-email" class="widefat" value="<?php echo esc_attr( $contact['email'] ); ?>" />
	</p>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_location_meta_box_nonce' );
}
add_action( 'cartography_location_meta_box_contact_fields', 'cartography_location_render_contact_email_field', 10 );


/**
 * Location contact website
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_location_render_contact_website_field( $post_id = 0 ) {
	$contact = get_post_meta( $post_id, 'location_contact', true );
	?>
	<p>
		<label class="cartography-label" for="cartography-location-contact-website"><?php esc_html_e( 'Website:', 'cartography' ); ?></label>
		<input type="url" name="location_contact[website]" id="cartography-location-contact-website" class="widefat" value="<?php echo esc_attr( $contact['website'] ); ?>" />
	</p>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_location_meta_box_nonce' );
}
add_action( 'cartography_location_meta_box_contact_fields', 'cartography_location_render_contact_website_field', 10 );


/**
 * Location contact phone
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_location_render_contact_phone_field( $post_id = 0 ) {
	$contact = get_post_meta( $post_id, 'location_contact', true );
	?>
	<p>
		<label class="cartography-label" for="cartography-location-contact-phone"><?php esc_html_e( 'Phone Number:', 'cartography' ); ?></label>
		<input type="tel" name="location_contact[phone]" id="cartography-location-contact-phone" class="widefat" value="<?php echo esc_attr( $contact['phone'] ); ?>" />
	</p>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_location_meta_box_nonce' );
}
add_action( 'cartography_location_meta_box_contact_fields', 'cartography_location_render_contact_phone_field', 10 );


/**
 * Save post meta when the save_post action is called
 *
 * @since       1.0.0
 * @param       int $post_id The ID of the post being saved.
 * @return      void
 */
function cartography_save_location( $post_id = 0 ) {
	$query_post = wp_unslash( $_POST );

	// Don't process if nonce can't be validated.
	if ( ! isset( $query_post['cartography_location_meta_box_nonce'] ) ) {
		return;
	}

	$meta_box_nonce = wp_strip_all_tags( $query_post['cartography_location_meta_box_nonce'] );

	if ( ! wp_verify_nonce( $meta_box_nonce, basename( __FILE__ ) ) ) {
		return;
	}

	// Don't process if this is an auto-save.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		return;
	}

	// Don't process for bulk edits.
	if ( isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	global $post;

	// Don't process if this is a revision.
	if ( isset( $post->post_type ) && 'revision' === $post->post_type ) {
		return;
	}

	// The default fields that get saved.
	$fields = apply_filters(
		'cartography_location_meta_box_fields_save',
		array(
			'location_address',
			'location_contact',
			'location_coords',
		)
	);

	foreach ( $fields as $field ) {
		if ( array_key_exists( $field, $query_post ) && isset( $query_post[ $field ] ) ) {
			$field      = wp_unslash( $field );
			$post_field = $query_post[ $field ];

			if ( is_string( $post_field ) ) {
				$post_field = sanitize_text_field( $post_field );
			}

			$new = apply_filters( 'cartography_location_meta_box_save_' . $field, $post_field );

			update_post_meta( $post_id, $field, $new );
			continue;
		}

		delete_post_meta( $post_id, $field );
	}
}
add_action( 'save_post', 'cartography_save_location' );
