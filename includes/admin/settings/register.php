<?php
/**
 * Register settings
 *
 * @package     Cartography\Admin\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function cartography_add_menu( $menu ) {
	$menu['type']       = 'submenu';
	$menu['parent']     = 'edit.php?post_type=map';
	$menu['page_title'] = __( 'Cartography Settings', 'cartography' );
	$menu['menu_title'] = __( 'Settings', 'cartography' );
	$menu['show_title'] = true;
	$menu['position']   = 40;

	return $menu;
}
add_filter( 'cartography_menu', 'cartography_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function cartography_settings_tabs( $tabs ) {
	$tabs['providers'] = __( 'Providers', 'cartography' );
	$tabs['support']   = __( 'Support', 'cartography' );

	return $tabs;
}
add_filter( 'cartography_settings_tabs', 'cartography_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function cartography_registered_settings_sections( $sections ) {
	// TODO: Make Welcome! only show on initial install and updates.
	$sections = array(
		'providers' => array(
			'google' => __( 'Google Maps', 'cartography' ),
		),
		'support'   => array(),
	);

	return $sections;
}
add_filter( 'cartography_registered_settings_sections', 'cartography_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function cartography_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'cartography_unsavable_tabs', 'cartography_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function cartography_define_unsavable_sections() {
	$sections = array( 'core/welcome' );

	return $sections;
}
add_filter( 'cartography_unsavable_sections', 'cartography_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function cartography_registered_settings( $settings ) {
	$core_settings = array(
		'providers' => apply_filters(
			'cartography_registered_settings_providers',
			array(
				'google' => array(
					array(
						'id'   => 'google_maps_header',
						'name' => __( 'Google Maps', 'cartography' ),
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'google_maps_api_key',
						'name' => __( 'API Key', 'cartography' ),
						'desc' => sprintf(
							/* translators: %1$s represents the link anchor opening tag, %2$s represents the closing tag. */
							__( 'Your Google Maps Javascript API key. %1$s(Click here to learn more)%2$s', 'cartography' ),
							'<a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">',
							'</a>'
						),
						'type' => 'password',
					),
					array(
						'id'      => 'google_maps_view',
						'name'    => __( 'Default Map View', 'cartography' ),
						'desc'    => __( 'The default map view.', 'cartography' ),
						'type'    => 'select',
						'options' => array(
							'default'   => __( 'Default Map', 'cartography' ),
							'satellite' => __( 'Satellite Map', 'cartography' ),
							'hybrid'    => __( 'Hybrid Map', 'cartography' ),
							'terrain'   => __( 'Terrain Map', 'cartography' ),
						),
						'std'     => 'default',
					),
					array(
						'id'   => 'google_maps_zoom',
						'name' => __( 'Default Map Zoom', 'cartography' ),
						'desc' => __( 'The default zoom level.', 'cartography' ),
						'type' => 'number',
						'std'  => 8,
						'min'  => 0,
						'step' => 1,
					),
					array(
						'id'   => 'google_maps_pin',
						'name' => __( 'Default Map Pin', 'cartography' ),
						'desc' => __( 'The default map pin.', 'cartography' ),
						'type' => 'upload',
					),
					array(
						'id'   => 'snazzy_maps_header',
						'name' => __( 'Snazzy Maps', 'cartography' ),
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'snazzy_maps_style',
						'name' => __( 'Map Style', 'cartography' ),
						'desc' => sprintf(
							/* translators: %s represents the link to Snazzy Maps. */
							__( 'Map style from %s.', 'cartography' ),
							'<a href="https://snazzymaps.com/explore" target="_blank">Snazzy Maps</a>'
						),
						'type' => 'html',
						'mode' => 'JavaScript',
					),
				),
			),
		),
	);

	$core_settings = apply_filters( 'cartography_registered_settings_modules', $core_settings );

	$support_settings = array(
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Cartography Support', 'cartography' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'cartography' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $core_settings, $support_settings );
}
add_filter( 'cartography_registered_settings', 'cartography_registered_settings' );
