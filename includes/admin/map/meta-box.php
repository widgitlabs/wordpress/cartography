<?php
/**
 * Map meta boxes
 *
 * @package     Cartography\Map\MetaBoxes
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Add map meta boxes.
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_map_add_meta_boxes() {
	add_meta_box(
		'cartography-map-locations',
		__( 'Map Locations', 'cartography' ),
		'cartography_map_render_locations_meta_box',
		'map',
		'normal',
		'high'
	);
}
add_action( 'add_meta_boxes', 'cartography_map_add_meta_boxes' );


/**
 * Map locations meta box
 *
 * @since       1.0.0
 * @return      void
 */
function cartography_map_render_locations_meta_box() {
	global $post;

	do_action( 'cartography_map_meta_box_locations_fields', $post->ID );
}


/**
 * Map locations
 *
 * @since       1.0.0
 * @param       int $post_id The WordPress post ID.
 * @return      void
 */
function cartography_map_render_locations_fields( $post_id = 0 ) {
	?>
	<?php
	wp_nonce_field( basename( __FILE__ ), 'cartography_map_meta_box_nonce' );
}
add_action( 'cartography_map_meta_box_locations_fields', 'cartography_map_render_locations_fields', 10 );


/**
 * Save post meta when the save_post action is called
 *
 * @since       1.0.0
 * @param       int $post_id The ID of the post being saved.
 * @return      void
 */
function cartography_save_map( $post_id = 0 ) {
	$query_post = wp_unslash( $_POST );

	// Don't process if nonce can't be validated.
	if ( ! isset( $query_post['cartography_map_meta_box_nonce'] ) ) {
		return;
	}

	$meta_box_nonce = wp_strip_all_tags( $query_post['cartography_map_meta_box_nonce'] );

	if ( ! wp_verify_nonce( $meta_box_nonce, basename( __FILE__ ) ) ) {
		return;
	}

	// Don't process if this is an auto-save.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		return;
	}

	// Don't process for bulk edits.
	if ( isset( $_REQUEST['bulk_edit'] ) ) {
		return;
	}

	global $post;

	// Don't process if this is a revision.
	if ( isset( $post->post_type ) && 'revision' === $post->post_type ) {
		return;
	}

	// The default fields that get saved.
	$fields = apply_filters(
		'cartography_map_meta_box_fields_save',
		array()
	);

	foreach ( $fields as $field ) {
		if ( array_key_exists( $field, $query_post ) && isset( $query_post[ $field ] ) ) {
			$field      = wp_unslash( $field );
			$post_field = $query_post[ $field ];

			if ( is_string( $post_field ) ) {
				$post_field = sanitize_text_field( $post_field );
			}

			$new = apply_filters( 'cartography_map_meta_box_save_' . $field, $post_field );

			update_post_meta( $post_id, $field, $new );
			continue;
		}

		delete_post_meta( $post_id, $field );
	}
}
add_action( 'save_post', 'cartography_save_map' );
