# Cartography

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wordpress/cartography/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wordpress/cartography/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wordpress/cartography/pipelines)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

A better map builder.

## Description

Cartography has been a long time coming. Over the course of a year, I've used
four different map plugins and written two custom mapping functions for various
clients. The decision to use each of those solutions wasn't made due to client
preference or new plugin releases; rather, in each case the decision was based
on nothing more than the specific needs of the client. Maps really aren't that
overly complicated things - why do I need six solutions for six clients? Even
more annoying to me personally, why do map plugins all try to build clever
interfaces? So Cartography is an attempt to roll the most comprehensive,
streamlined, and clean map builder possible for WordPress.

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/cartography/issues)!
