/*global jQuery, document, alert, ajaxurl, google*/
jQuery(document).ready(function($) {
    'use strict';

    var geocoder, map, marker, pin;

    /**
     * Location information meta box
     */
    var Cartography_Location_Info_Meta_Box = {
        init: function() {
            this.renderMap();
            this.setLocation();
            this.updateLatitude();
            this.updateLongitude();
            this.updateStatesList();
            this.toggleAdvancedOptions();
            this.addSelect2();
        },
        renderMap: function() {
            if ($('#cartography-location-map').length) {
                var mapElement = document.getElementById('cartography-location-map');
                var lat = $('#cartography-location-coords-latitude').val();
                var lng = $('#cartography-location-coords-longitude').val();

                var type = $('#cartography-location-map').data('type');
                var mapType = google.maps.MapTypeId.ROADMAP;

                if (type === 'satellite') {
                    mapType = google.maps.MapTypeId.SATELLITE;
                } else if (type === 'hybrid') {
                    mapType = google.maps.MapTypeId.HYBRID;
                } else if (type === 'terrain') {
                    mapType = google.maps.MapTypeId.TERRAIN;
                }

                pin = $('#cartography-location-map').data('pin');

                var mapOptions = {
                    zoom: parseInt($('#cartography-location-map').data('zoom')),
                    center: new google.maps.LatLng(37.2523968, -121.5491826),
                    mapTypeId: mapType
                };

                map = new google.maps.Map(mapElement, mapOptions);

                if (lat.length && lng.length) {
                    var coords = new google.maps.LatLng(lat, lng);

                    map.setCenter(coords);

                    marker = new google.maps.Marker({
                        map: map,
                        position: coords,
                        title: $('#title').val(),
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    });

                    if (pin.length) {
                        marker.setIcon(pin);
                    }
                }
            }
        },
        setLocation: function() {
            if ($('#cartography-location-set').length) {
                $('#cartography-location-set').on('click', function(event) {
                    event.preventDefault();

                    var address1 = $('#cartography-location-address1').val();
                    var address2 = $('#cartography-location-address2').val();
                    var city = $('#cartography-location-city').val(),
                        state;
                    var zip = $('#cartography-location-zip').val();
                    var country = $('#cartography-location-country').val();

                    if ('input' === $(this).prop('nodeName')) {
                        state = $('#cartography-location-state').val();
                    } else {
                        state = $('#cartography-location-state').val();
                    }

                    var address = address1 + ' ' + address2 + ' ' +
                        city + ' ' + state + ' ' + zip + ' ' + country;

                    address = address.trim();

                    geocoder = new google.maps.Geocoder();

                    geocoder.geocode({ 'address': address }, function(results, status) {
                        if (status === 'OK') {
                            if (marker) {
                                marker.setMap(null);
                            }

                            map.setCenter(results[0].geometry.location);
                            marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location,
                                title: $('#title').val(),
                                draggable: true,
                                animation: google.maps.Animation.DROP
                            });

                            if (pin.length) {
                                marker.setIcon(pin);
                            }

                            google.maps.event.addListener(marker, 'dragend', function() {
                                $('#cartography-location-coords-latitude').val(marker.getPosition().lat());
                                $('#cartography-location-coords-longitude').val(marker.getPosition().lng());
                            });

                            $('#cartography-location-coords-latitude').val(results[0].geometry.location.lat());
                            $('#cartography-location-coords-longitude').val(results[0].geometry.location.lng());
                        } else {
                            alert('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                });
            }
        },
        updateLatitude: function() {
            if ($('#cartography-location-coords-latitude').length) {
                $('#cartography-location-coords-latitude').on('change', function() {
                    var lat = $('#cartography-location-coords-latitude').val();
                    var lng = $('#cartography-location-coords-longitude').val();

                    if (lat.length && lng.length) {
                        var coords = new google.maps.LatLng(lat, lng);

                        if (marker) {
                            marker.setMap(null);
                        }

                        map.setCenter(coords);

                        marker = new google.maps.Marker({
                            map: map,
                            position: coords,
                            title: $('#title').val(),
                            draggable: true,
                            animation: google.maps.Animation.DROP
                        });
                    }
                });
            }
        },
        updateLongitude: function() {
            if ($('#cartography-location-coords-longitude').length) {
                $('#cartography-location-coords-longitude').on('change', function() {
                    var lat = $('#cartography-location-coords-latitude').val();
                    var lng = $('#cartography-location-coords-longitude').val();

                    if (lat.length && lng.length) {
                        var coords = new google.maps.LatLng(lat, lng);

                        if (marker) {
                            marker.setMap(null);
                        }

                        map.setCenter(coords);

                        marker = new google.maps.Marker({
                            map: map,
                            position: coords,
                            title: $('#title').val(),
                            draggable: true
                        });
                    }
                });
            }
        },
        updateStatesList: function() {
            if ($('#cartography-location-country').length) {
                $('#cartography-location-country').change(function() {
                    var $this = $(this);
                    var data = {
                        action: 'cartography_get_states',
                        country: $this.val(),
                        nonce: $this.data('nonce'),
                    };

                    $('#cartography-location-state').next('.select2-container').remove();

                    $.post(ajaxurl, data, function(response) {
                        if ('no_states' === response) {
                            $('#cartography-location-state').replaceWith('<input type="text" name="location_address[state]" id="cartography-location-state" value="" />');
                        } else {
                            $('#cartography-location-state').replaceWith(response);
                        }
                    });

                    return false;
                });
            }
        },
        toggleAdvancedOptions: function() {
            if ($('#cartography-location-coords-toggle').length) {
                $('#cartography-location-coords-toggle').click(function(event) {
                    event.preventDefault();

                    var advancedOptions = $('#cartography-location-coords-wrap');

                    if (advancedOptions.is(':visible')) {
                        advancedOptions.slideUp();
                    } else {
                        advancedOptions.slideDown();
                    }
                });
            }
        },
        addSelect2: function() {
            $('.cartography-select2').select2();
        }
    };
    Cartography_Location_Info_Meta_Box.init();
});