/*global jQuery, document, google, alert*/
jQuery(document).ready(function($) {
    'use strict';

    var map, locations, theRadius, radiusCircle;

    /**
     * Location information meta box
     */
    var AppPresser_Maps = {
        init: function() {
            this.renderMap();
            this.targetLocation();
            this.getLocation();
            this.filterLocations();
        },
        renderMap: function() {
            if ($('#cartography-location-map').length) {
                var mapElement = document.getElementById('cartography-location-map');

                var zoom = $('#cartography-location-map').attr('data-zoom');
                var type = $('#cartography-location-map').attr('data-maptype');
                var pin = $('#cartography-location-map').attr('data-pin');
                var mapStyle = JSON.parse($('#cartography-location-map').attr('data-style'));
                var mapType = google.maps.MapTypeId.ROADMAP;

                if (type === 'satellite') {
                    mapType = google.maps.MapTypeId.SATELLITE;
                } else if (type === 'hybrid') {
                    mapType = google.maps.MapTypeId.HYBRID;
                } else if (type === 'terrain') {
                    mapType = google.maps.MapTypeId.TERRAIN;
                }

                var mapOptions = {
                    zoom: parseInt(zoom),
                    center: new google.maps.LatLng(37.2523968, -121.5491826),
                    mapTypeId: mapType,
                    styles: mapStyle
                };

                map = new google.maps.Map(mapElement, mapOptions);
                locations = [];

                var latLngBounds = new google.maps.LatLngBounds();

                $('.cartography-location-wrap').each(function() {
                    var title = $(this).find('.cartography-location-title-link').attr('title');
                    var lat = $(this).attr('data-latitude');
                    var lng = $(this).attr('data-longitude');

                    var coords = new google.maps.LatLng(lat, lng);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: coords,
                        title: title,
                        animation: google.maps.Animation.DROP
                    });

                    if (pin.length) {
                        marker.setIcon(pin);
                    }

                    var infoWindow = new google.maps.InfoWindow();
                    var infoWindowDetails = $(this).find('.cartography-location-details-wrap').html();

                    infoWindow.setContent(infoWindowDetails);

                    latLngBounds.extend(marker.position);

                    locations.push({
                        id: $(this).attr('data-id'),
                        lat: lat,
                        lng: lng,
                        marker: marker,
                        infoWindow: infoWindow,
                        distance: '0',
                        visible: true
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        map.setCenter(coords);

                        $.each(locations, function(key, location) {
                            location.infoWindow.close(map, location.marker);
                        });

                        infoWindow.open(map, marker);
                    });
                });

                map.setCenter(latLngBounds.getCenter());
                map.fitBounds(latLngBounds);
            }
        },
        targetLocation: function() {
            if ($('.cartography-location-title-link').length) {
                $('.cartography-location-title-link').on('click', function(event) {
                    event.preventDefault();

                    var id = $(this).attr('data-id');

                    $.each(locations, function(key, location) {
                        location.infoWindow.close(map, location.marker);

                        if (location.id === id) {
                            var lat = parseInt($(this).parent().next('.cartography-location-address').find('span').attr('data-latitude'));
                            var lng = parseInt($(this).parent().next('.cartography-location-address').find('span').attr('data-longitude'));
                            var coords = new google.maps.LatLng(lat, lng);

                            map.setCenter(coords);

                            location.infoWindow.open(map, location.marker);
                        }
                    });
                });
            }
        },
        getLocation: function() {
            if ($('#cartography-get-location').length) {
                $('#cartography-get-location').on('click', function() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(appPresserMapsShowPosition);
                    }
                });
            }
        },
        filterLocations: function() {
            if ($('#cartography-filter-submit').length) {
                $('#cartography-filter-submit').on('click', function() {
                    var radius = parseInt($('#cartography-filter-radius').val());

                    if ($('#cartography-filter-location').val().length && radius > 0) {
                        var destinations = [];

                        var geocoder = new google.maps.Geocoder();

                        geocoder.geocode({ 'address': $('#cartography-filter-location').val() }, function(results, status) {
                            if (status === 'OK') {
                                map.setCenter(results[0].geometry.location);

                                if (theRadius) {
                                    theRadius.setMap(null);
                                }

                                radiusCircle = {
                                    strokeColor: '#9a1b1e',
                                    strokeOpacity: 0.8,
                                    strokeWeight: 2,
                                    fillColor: '#9a1b1e',
                                    fillOpacity: 0.35,
                                    map: map,
                                    center: results[0].geometry.location,
                                    radius: radius * 1609.344 // in meters
                                };

                                theRadius = new google.maps.Circle(radiusCircle);

                                $.each(locations, function(key, location) {
                                    destinations.push(new google.maps.LatLng(location.lat, location.lng));
                                });

                                var service = new google.maps.DistanceMatrixService();

                                service.getDistanceMatrix({
                                    origins: [$('#cartography-filter-location').val()],
                                    destinations: destinations,
                                    travelMode: 'DRIVING',
                                    unitSystem: google.maps.UnitSystem.IMPERIAL,
                                }, appPresserMapsFilterLocations);
                            } else {
                                alert('Geocode was not successful for the following reason: ' + status);
                            }
                        });
                    } else {
                        var latLngBounds = new google.maps.latLngBounds();

                        if ($('.cartography-no-locations').is(':visible')) {
                            $('.cartography-no-locations').fadeOut('fast', function() {
                                $(this).attr('display', 'none');

                                $.each(locations, function(key, location) {
                                    var listItem = $('#cartography-location-' + location.id);

                                    if (listItem.not(':visible')) {
                                        location.marker.setMap(map);

                                        latLngBounds.extend(location.marker.position);

                                        listItem.fadeIn('fast', function() {});
                                    }
                                });

                                theRadius.setMap(null);

                                map.setCenter(latLngBounds.getCenter());
                                map.fitBounds(latLngBounds);
                            });
                        } else {
                            $.each(locations, function(key, location) {
                                var listItem = $('#cartography-location-' + location.id);

                                if (listItem.not(':visible')) {
                                    location.marker.setMap(map);

                                    latLngBounds.extend(location.marker.position);

                                    listItem.fadeIn('fast', function() {});
                                }
                            });

                            theRadius.setMap(null);

                            map.setCenter(latLngBounds.getCenter());
                            map.fitBounds(latLngBounds);
                        }
                    }
                });
            }
        }
    };
    AppPresser_Maps.init();

    function appPresserMapsShowPosition(position) {
        var lat = position.coords.latitude;
        var lang = position.coords.longitude;
        var url = cartography_vars.geocode_api + '&latlng=' + lat + ',' + lang + '&sensor=true';

        $.getJSON(url, function(data) {
            var address = data.results[0].formatted_address;

            $('#cartography-filter-location').val(address);
        });
    }

    function appPresserMapsFilterLocations(response, status) {
        if (status === 'OK') {
            var results = response.rows[0].elements;

            $.each(locations, function(key, location) {
                var element = results[key];
                var distance = element.distance.text;

                location.distance = distance.replace(' mi', '').replace(',', '');

                locations[key] = location;
            });

            var radius = parseInt($('#cartography-filter-radius').val());
            // var visible = locations.length;
            var latLngBounds = new google.maps.latLngBounds();

            $.each(locations, function(key, location) {
                var listItem = $('#cartography-location-' + location.id);

                if (parseInt(location.distance) > radius) {
                    location.marker.setMap(null);

                    locations[key].visible = false;

                    if (listItem.is(':visible')) {
                        listItem.fadeOut('fast', function() {
                            $(this).attr('display', 'none');
                        });
                    }
                } else {
                    location.marker.setMap(map);

                    locations[key].visible = true;

                    latLngBounds.extend(location.marker.position);

                    if (listItem.not(':visible')) {
                        listItem.fadeIn('fast', function() {});
                    }
                }
            });

            var theLocations = Object.entries(locations);
            var visibleLocations = theLocations.filter(function(key, value) {
                return true === value.visible;
            });

            var visible = Object.fromEntries(visibleLocations);

            if (Object.keys(visible).length > 0) {
                if ($('.cartography-no-locations').is(':visible')) {
                    $('.cartography-no-locations').fadeOut('fast', function() {});
                }

                map.setCenter(latLngBounds.getCenter());

                if (Object.keys(visible).length > 1) {
                    map.fitBounds(latLngBounds);
                } else {
                    if (radius === 50) {
                        map.setZoom(8);
                    } else if (radius === 100) {
                        map.setZoom(7);
                    } else if (radius === 200) {
                        map.setZoom(6);
                    }
                }
            } else {
                if ($('.cartography-no-locations').not(':visible')) {
                    $('.cartography-no-locations').fadeIn('fast', function() {});
                }

                if (radius === 50) {
                    map.setZoom(8);
                } else if (radius === 100) {
                    map.setZoom(7);
                } else if (radius === 200) {
                    map.setZoom(6);
                }
            }
        }
    }
});
